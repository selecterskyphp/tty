<?php
define('TTY', true);
define('UPLOAD_PATH', './Uploads/');
define('VERSION', '');
define('UPDATETIME', '');
$database = require ('./config.php');
$config	= array(

		'DEFAULT_CHARSET' => 'utf-8',
        'PAGE_LISTROWS'=>2,//默认分页条数
		'APP_DEBUG' => false,
		'APP_GROUP_LIST' => 'Home,Admin,User,Blog',
		'DEFAULT_GROUP' =>'Home',
		'TMPL_FILE_DEPR' => '_',
		'DB_FIELDS_CACHE' => false,
		'DB_FIELDTYPE_CHECK' => true,
		'URL_CASE_INSENSITIVE'=>false,//原来是 true.网站不自动转换小写 modify cjx of 2011119
		'URL_ROUTER_ON' => true,
		'URL_AUTO_REDIRECT' => false,
		'DEFAULT_LANG'   =>'zh-cn',//默认语言
		'LANG_SWITCH_ON' => true, //多语言
		//'TMPL_DETECT_THEME'     => true,
		'TAG_EXTEND_PARSE' =>array(
			'if' => 'template_if',
			'else' =>'template_else',
			'elseif' => 'template_elseif'
		),
		'APP_AUTOLOAD_PATH'=> 'Think.Util.,@.TagLib.',
		'TAGLIB_LOAD' => true,
		'TAGLIB_PRE_LOAD' => 'html,yp',
        'HTML_PATH'=>'./',
		'TMPL_PARSE_STRING'		=>array(
			'../Public' => __ROOT__.'/Tty/Tpl/'.C('DEFAULT_THEME').'/Public',
			'__TMPL__' => __ROOT__.'/Tty/Tpl/'.C('DEFAULT_THEME').'/Home/'
		),
		'DEVICES_STATUS'=>array('<font color=green>正常</font>','检测中','未检测','<font color=red>故障</font>'),
		'FIRE_STATUS'=>array(1=>'火警',2=>'测试',3=>'误报'),
		'TIME'=>array('minute'=>60,'hour'=>3600,'day'=>86400,'month'=>2592000,'year'=>31104000),
		'BATTERY_VALID'=>180  //电池有效时间 单位天
);
return array_merge($database, $config);
?>
<?php
return array(
	'devices_name' => '设备名称',
	'devices_user' => '所属商户',
	'devices_devgroup' => '所属群组',
	'devices_firegroup' => '所属消防组',
	'devices_status' => '当前状态',
	'devices_alphanum' => '传感器个数',
);
?>
<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class AlphaAction extends AdminbaseAction {

    public $dao,$devM,$detect;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Home.'.MODULE_NAME);
		$this->devM = D('Home.Devices');
		$this->detect = D('Home.AlphaDetect');
	}


	function index(){
		import ( '@.ORG.Page' );

		$keyword=$_GET['keyword'];
		$searchtype=$_GET['searchtype'];
		$groupid =intval($_GET['groupid']);

		$this->assign($_GET);
		
		if(!empty($keyword) && !empty($searchtype)){
			$where[$searchtype]=array('like','%'.$keyword.'%');
		}

		$user=$this->dao;
		$count=$user->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$user->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$tmp = array();
		$umod = D('Home.User');
		foreach($list as $v)
		{
			$name = $this->devM->where('telecomphoneinfo_id='.$v['telecomphoneinfo_id'])->getField('name');
			$v['devicesname']=$name;
			$status=C('DEVICES_STATUS');
			$result = $this->detect->where('fogsensordetect_type=1 and fogsensorinfo_id='.$v['id'])->getField('result');
			//var_dump($result);
			if(!$result)
			{
			    $result = 2;//未知
			}
			$v['statusname']=$status[$result];
			$name = $umod->where('userinfo_id='.$v['userinfo_id'])->getField('name');
			$v['username']=$name;
			$tmp[]=$v;
		}
		$this->assign('ulist',$tmp);
		$this->display();
	}

	function _before_add(){
	    $umod = D('Home.User');
	    $list = $umod->where('userrole_id=3')->field('id,name')->select();
	    
		$this->assign('ulist',json_encode($list));
		$list = $this->devM->field('id,name,userinfo_id')->select();
		$this->assign('glist',json_encode($list));
	}
	

	function _before_edit(){
		$umod = D('Home.User');
	    $list = $umod->where('userrole_id=3')->field('id,name')->select();
	    
		$this->assign('ulist',json_encode($list));
		$list = $this->devM->field('id,name,userinfo_id')->select();
		$this->assign('glist',json_encode($list));
	}
}
?>
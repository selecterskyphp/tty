<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class DevicesAction extends AdminbaseAction {

    public $dao,$devM,$fireM,$userM;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Home.'.MODULE_NAME);
		$this->devM = D('Home.DevicesGroup');
		$this->fireM = D('Home.FireGroup');
		$this->userM = D('Home.User');
	}


	function index(){
		import ( '@.ORG.Page' );

		$keyword=$_GET['keyword'];
		$searchtype=$_GET['searchtype'];
		$groupid =intval($_GET['groupid']);

		$this->assign($_GET);
		
		if(!empty($keyword) && !empty($searchtype)){
			$where[$searchtype]=array('like','%'.$keyword.'%');
		}

		$user=$this->dao;
		$count=$user->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$user->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$tmp = array();
		$alpha = D('Home.Alpha');
		$detect = D('Home.DevicesDetect');
		foreach ($list as $v)
		{
			$name = $this->devM->where('firecallergroup_id='.$v['firecallergroup_id'])->getField('name');
			$v['devgroupname']=$name;
			$name = $this->fireM->where('firecontrollergroup_id='.$v['firecontrollergroup_id'])->getField('name');
			$v['firegroupname']=$name;
			$name=$this->userM->where('userinfo_id='.$v['userinfo_id'])->getField('name');
			$v['username']=$name;
			$num = $alpha->where('telecomphoneinfo_id='.$v['id'])->count();
			$v['alphanum']=$num;
			$status=C('DEVICES_STATUS');
			$result = $detect->where('telecomphonedetect_type=1 and telecomphoneinfo_id='.$v['id'])->getField('result');
			//var_dump($result);
			if(!$result)
			{
			    $result = 2;//未知
			}
			$v['statusname']=$status[$result];
			$tmp[]=$v;
		}
		$this->assign('ulist',$tmp);
		$this->display();
	}

	function _before_add(){
		$this->assign('devgroup',$this->devM->findAll());
		$this->assign('firegroup',$this->fireM->findAll());
		$this->assign("user",$this->userM->where('userrole_id=3')->field('id,name')->select());
	}

	function _before_edit(){
		$this->assign('devgroup',$this->devM->findAll());
		$this->assign('firegroup',$this->fireM->findAll());
		$this->assign("user",$this->userM->where('userrole_id=3')->field('id,name')->select());
	}
}
?>
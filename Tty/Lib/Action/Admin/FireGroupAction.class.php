<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class FireGroupAction extends AdminbaseAction {

    public $dao;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Home.'.MODULE_NAME);
	}


	function index(){
		import ( '@.ORG.Page' );

		$keyword=$_GET['keyword'];
		$searchtype=$_GET['searchtype'];
		$groupid =intval($_GET['groupid']);

		$this->assign($_GET);
		
		if(!empty($keyword) && !empty($searchtype)){
			$where[$searchtype]=array('like','%'.$keyword.'%');
		}

		$user=$this->dao;
		$count=$user->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$user->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();

		$tmp = array();
        $mod = D('Home.User');
        foreach($list as $v)
        {
            $name = $mod->where('userinfo_id='.$v['userinfo_id'])->getField('name');
            $v['userinfo_name'] = $name;
            $tmp[]=$v;
        }
		$this->assign('ulist',$tmp);
		$this->display();
	}
    function edit()
	{
	    $mod = D('Home.User');
	    $where['userrole_id'] = 3;
	    $ulist =  $mod->where($where)->field('id,name')->select();
	    //var_dump($ulist,$mod->getLastSql());
	    $this->assign('ulist',$ulist);
	    parent::edit();
	}
    function add()
	{
	    $mod = D('Home.User');
	    $where['userrole_id'] = 3;
	    $ulist =  $mod->where($where)->field('id,name')->select();
	    //var_dump($ulist,$mod->getLastSql());
	    $this->assign('ulist',$ulist);
	    parent::add();
	}
}
?>
<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class FirePeopleAction extends AdminbaseAction {

    public $dao,$groupM;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Home.'.MODULE_NAME);
		$this->groupM=D('Home.FireGroup');
	}


	function index(){
		import ( '@.ORG.Page' );

		$keyword=$_GET['keyword'];
		$searchtype=$_GET['searchtype'];
		$groupid =intval($_GET['groupid']);

		$this->assign($_GET);
		
		if(!empty($keyword) && !empty($searchtype)){
			$where[$searchtype]=array('like','%'.$keyword.'%');
		}

		$user=$this->dao;
		$count=$user->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$user->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$tmp = array();
		$mod = D('Home.User');
		foreach ($list as $v)
		{
			$name = $this->groupM->where('firecontrollergroup_id='.$v['firecontrollergroup_id'])->getField('name');
			$v['groupname']=$name;
			$name = $mod->where('userinfo_id='.$v['userinfo_id'])->getField('name');
			$v['username']=$name;
			$tmp[]=$v;
		}
		$this->assign('ulist',$tmp);
		$this->display();
	}
	
    function setmain()
	{
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $gid = isset($_GET['gid'])?intval($_GET['gid']):0;
	    $mod = $this->dao;
	    $data['ismain']=0;
	    $where['firecontrollergroup_id']=$gid;
	    $mod->where($where)->data($data)->save();
	    //var_dump($mod->getLastSql());
	    $data['ismain']=1;
	    unset($where);
	    $where['id']=$id;
	    $mod->where($where)->data($data)->save();
	    //var_dump($mod->getLastSql());
	    //exit;
	    $this->redirect(U('FirePeople/index'));
	}

	function _before_add(){
	    $mod = D('Home.User');
	    $ulist = $mod->where('userrole_id=3')->select();
	    $this->assign('ulist',json_encode($ulist));
	    $glist = $this->groupM->select();
		$this->assign('glist',json_encode($glist));
	}

	function _before_edit(){
		$mod = D('Home.User');
	    $ulist = $mod->where('userrole_id=3')->select();
	    $this->assign('ulist',json_encode($ulist));
	    $glist = $this->groupM->select();
		$this->assign('glist',json_encode($glist));
	}
    function insert()
	{
		$model = $this->dao;
	    $where['ismain'] = 1;
	    $where['userinfo_id'] = isset($_POST['userinfo_id'])?intval($_POST['userinfo_id']):0;
	    $where['firecontrollergroup_id'] = isset($_POST['firecontrollergroup_id'])?intval($_POST['firecontrollergroup_id']):0;
	    $result = $model->where($where)->getField('id');
	    if(!$result)
	    {
	        //如果没有设置主号码，则自动将自动设置为主号码
	        $_POST['ismain']=1;
	    }
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
		if ($model->add() !==false) {
		    
			$this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
			$this->success (L('add_ok'));
		} else {
			$this->error (L('add_error').': '.$model->getDbError());
		}
	}
}
?>
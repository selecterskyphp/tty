<?php
/**
 * 
 * Posid (推荐位管理)
 *
 */
class ProvinceAction extends AdminbaseAction {

	protected $dao,$Type;
    function _initialize()
    {	
		parent::_initialize();
		$this->dao = D('Home.DomainInfo');
		$this->assign($_GET);

    }
    public function index()
    {
        $parentid = isset($_REQUEST['parentid'])?intval($_REQUEST['parentid']):0;
        $where['parentsid'] = $parentid;
        $list = $this->dao->where($where)->select();
        $this->assign('list', $list);
        $this->assign('parentid',$parentid);
        $this->display();
    }
    public function auto()
    {
        $this->dao->execute('truncate table tbl_domain_info');
        $data = array();
        $data['name'] = '浙江';
        $data['parentsid'] = 0;
        $data['rank'] = 0;
        $data['rankpath'] = 0;
        $data['enable'] = 1;
        $data['order'] = 0;
        $this->dao->data($data)->add();
        $cityM = M('city');
        $areaM = M('district');
        $city = $cityM->where('cityUpId=11')->select();
        //var_dump($cityM->getLastSql());
        //exit;
        foreach ($city as $v)
        {
            $data = array();
            $data['name'] = $v['cityName'];
            $data['parentsid'] = 1;
            $data['rank'] = 1;
            $data['rankpath'] = 1;
            $data['enable'] = 1;
            $data['order'] = 0;
            $id=$this->dao->data($data)->add();
            $area = $areaM->where('districtUpId='.$v['cityId'])->select();
            foreach ($area as $vv)
            {
                $data = array();
                $data['name'] = $vv['districtName'];
                $data['parentsid'] = $id;
                $data['rank'] = 2;
                $data['rankpath'] = '1,'.$id;
                $data['enable'] = 1;
                $data['order'] = 0;
                $this->dao->data($data)->add();
            }
        }
       die('done!');
    }

	public function _before_add()
    {
		 
		$parentid =	intval($_GET['parentid']);
		if($parentid)
		{
			$name = $this->dao->where('domaininfo_id='.$parentid)->getField('name');
		}
		else
		{
			$name = '顶级菜单';
		}
		$this->assign('parent_name',$name);
	}
    
	public function edit()
    {
		$typeid = intval($_GET['parentid']);
		if($typeid)
		{
			$name = $this->dao->where('domaininfo_id='.$typeid)->getField('name');
		}
		else
		{
			$name = '顶级菜单';
		}
		$id = intval($_GET['id']);
		$vo = $this->dao->getBydomaininfo_id($id);
		//var_dump($this->dao->getLastSql());
		
		$this->assign('parent_name',$name);
		$this->assign('vo',$vo);
		$this->display();
	}
	
	public function update()
	{
	    $model = $this->dao;
	    $where['id'] = intval($_POST['id']);
	    $parentid = intval($_POST['parentsid']);
	    $data['name'] = trim($_POST['name']);
	    
	    $this->dao->where($where)->data($data)->save();
	    $this->assign ( 'jumpUrl', U(MODULE_NAME.'/index',array('parentid',$parentid)) );
	    $this->success (L('edit_ok'));
	}

	public function insert()
    {
		$parentid = intval($_POST['parentsid']);

		$model = $this->dao;
		if($parentid > 0)
		{
			$data = $model->where('domaininfo_id='.$parentid)->field('rank,rankpath')->find();
			$_POST['rank'] = $data['rank']+1;
			$_POST['rankpath'] = ('' ==$data['rankpath'])? $parentid : $data['rankpath'] . ',' . $parentid;
		}
		else 
		{
			$_POST['rank'] = 0;
			$_POST['rankpath'] = '';
		}
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
        $_POST['enable'] = 1;
        $_POST['order'] = 0;
		$typeid = $model->add() ;
		if ($typeid) {
			//$data['typeid'] = $data['keyid'] = $typeid; 
			$model->save($data);
			savecache($name);
			$this->assign ( 'jumpUrl', U(MODULE_NAME.'/index',array('parentid',$parentid)) );
			$this->success (L('add_ok'));
		} else {
			$this->error (L('add_error').': '.$model->getDbError());
		}
	}

	public function get_child($linkageid) {
		$where = array('parentsid'=>$linkageid);
		$this->childnode[] = intval($linkageid);
		$result = $this->db->select($where);
		if($result) {
			foreach($result as $r) {
				$this->_get_childnode($r['linkageid']);
			}
		}
	}

 

	public function get_arrparentids($pid, $array=array(),$arrparentid='') {
		if(!is_array($array) || !isset($array[$pid])) return $pid;
		$parentid = $array[$pid]['parentid'];
		$arrparentid = $arrparentid ? $parentid.','.$arrparentid : $parentid;
		if($parentid) {
			$arrparentid = $this->get_arrparentids($parentid,$array, $arrparentid);
		}else{
			$data = array();
			$data['bid'] = $pid;
			$data['arrparentid'] = $arrparentid;
		}
		return $data;
	}

	public function get_arrchildid($id, $array=array()) {
		$arrchildid = $id;
 
		foreach($array as $catid => $cat) {
			if($cat['parentid'] && $id != $catid) {
				$arrparentids = explode(',', $cat['arrparentid']);
				if(in_array($id, $arrparentids)) $arrchildid .= ','.$catid;
			}
		} 
		return $arrchildid;
	}
	
	public function status()
	{
	    $id = intval($_GET['id']);
	    $status = intval($_GET['status']);
	    $parentid = intval($_GET['parentid']);
	    $where['id']=$id;
	    $data['enable']=$status;
	    $this->dao->where($where)->data($data)->save();
	    $this->assign('jumpUrl',U('Province/index',array('parentid'=>$parentid)));
	    $this->success(L('do_success'));
	}
}
?>
<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class TelecomphoneInfoAction extends AdminbaseAction {

    public $dao,$devM,$fireM,$userM;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = M(MODULE_NAME);
		$this->devM = M('FireCallGroup');
		$this->fireM = M('FirecontrollerGroup');
		$this->userM = M('UserInfo');
	}


	function index(){
		import ( '@.ORG.Page' );

		$keyword=$_GET['keyword'];
		$searchtype=$_GET['searchtype'];
		$groupid =intval($_GET['groupid']);

		$this->assign($_GET);
		
		if(!empty($keyword) && !empty($searchtype)){
			$where[$searchtype]=array('like','%'.$keyword.'%');
		}

		$user=$this->dao;
		$count=$user->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$user->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$tmp = array();
		$alpha = M('alpha');
		foreach ($list as $v)
		{
			$name = $this->devM->where('firecallerinfo_id='.$v['firecallerinfo_id'])->getField('firecallinfo_name');
			$v['firecallerinfo_name']=$name;
			$name = $this->fireM->where('firecontrollerinfo_id='.$v['firecontrollerinfo_id'])->getField('firecontrollerinfo_name');
			$v['firecontrollerinfo_name']=$name;
			$name=$this->userM->where('id='.$v['userinfo_id'])->getField('userinfo_id');
			$v['userinfo_name']=$name;
			$num = $alpha->where('fogsensorinfo_id='.$v['fogsensorinfo_id'])->count();
			$v['fogsensorinfo_num']=$num;
			$status=$this->getDevicesStatus($v['telecomphoneinfo_id']);
			
			$v['statusname']=$status[$v['status']];
			$tmp[]=$v;
		}
		$this->assign('ulist',$tmp);
		$this->display();
	}

	function insert(){
		$user=$this->dao;
		if($data=$user->create()){
			if(false!==$user->add()){
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($user->getError());
		}
	}

	function update(){
		$user=$this->dao;
		if($data=$user->create()){
			if(!empty($data['id'])){
				if(false!==$user->save()){
					$this->success(L('edit_ok'));
				}else{
					$this->error(L('edit_error').$user->getDbError());
				}
			}else{
				$this->error(L('do_error'));
			}
		}else{
			$this->error($user->getError());
		}
	}
 

	function _before_add(){
		$this->assign('devgroup',$this->devM->findAll());
		$this->assign('firegroup',$this->fireM->findAll());
		$this->assign("user",$this->userM->where('groupid=3')->select());
		$this->assign("status",C('DEVICES_STATUS'));
	}

	function _before_edit(){
		$this->assign('devgroup',$this->devM->findAll());
		$this->assign('firegroup',$this->fireM->findAll());
		$this->assign("user",$this->userM->where('groupid=3')->select());
		$this->assign("status",C('DEVICES_STATUS'));
	}


	function delete(){
		$id=$_GET['id'];
		$user=$this->dao;
		if(false!==$user->delete($id)){
			$this->success(L('delete_ok'));
		}else{
			$this->error(L('delete_error').$user->getDbError());
		}
	}

	function deleteall(){		
		$ids=$_POST['ids'];
		if(!empty($ids) && is_array($ids)){
			$user=$this->dao;
			$id=implode(',',$ids);
			if(false!==$user->delete($id)){
				$this->success(L('delete_ok'));
			}else{
				$this->error(L('delete_error'));
			}
		}else{
			$this->error(L('do_empty'));
		}
	}
}
?>
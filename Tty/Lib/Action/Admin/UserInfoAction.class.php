<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class UserInfoAction extends AdminbaseAction {

    public $dao,$group,$domain;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Home.User');
		$this->group=D('Home.Role');
		$this->domain=D('Home.DomainInfo');
		$this->assign($_GET);
		$this->assign('usergroup',$this->group->select());
	}


	function index(){
		import ( '@.ORG.Page' );

		$keyword=$_GET['keyword'];
		$searchtype=$_GET['searchtype'];
		$groupid =intval($_GET['groupid']);
		
		if(!empty($keyword) && !empty($searchtype)){
			$where[$searchtype]=array('like','%'.$keyword.'%');
		}
		if($groupid)$where['userrole_id']=$groupid;

		$user=$this->dao;
		$count=$user->where($where)->count();
		$page=new Page($count,20);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$user->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
        for($i=0;$i<count($list);$i++)
        {
            $name = $this->group->where('userrole_id='.$list[$i]['userrole_id'])->getField('name');
            $list[$i]['userrole_name']=$name;
        }
		$this->assign('list',$list);
		$this->display();
	}

	function insert(){
		$user=$this->dao;
		if($data=$user->create()){
		if(isset($_POST['passwd']))
		{
		    if(strlen(trim($_POST['passwd']))>1)
		    {
		        $_POST['passwd'] = sysmd5($_POST['passwd']);
		    }
		    else
		    {
		        unset($_POST['passwd']);
		    }
		}
		if(isset($_POST['rankpath']))
		{
		    $_POST['rankpath'] = implode(',', $_POST['rankpath']);
		}
		unset($_POST['__hash__']);
			if(false!==$user->add($_POST)){
				$this->success(L('add_ok'));
			}else{
				$this->error(L('add_error'));
			}
		}else{
			$this->error($user->getError());
		}
	}

	function update(){
		$user=$this->dao;
		
		if($data=$user->create()){
    		if(isset($_POST['passwd']))
    		{
    		    if(strlen(trim($_POST['passwd']))>1)
    		    {
    		        $_POST['passwd'] = trim($_POST['passwd']);
    		        $_POST['passwd'] = sysmd5($_POST['passwd']);
    		    }
    		    else
    		    {
    		        unset($_POST['passwd']);
    		    }
    		}
    		if(isset($_POST['rankpath']))
    		{
    		    $_POST['rankpath'] = implode(',', $_POST['rankpath']);
    		}
		    unset($_POST['__hash__']);
			if(false!==$user->save($_POST)){
				$this->success(L('edit_ok'));
			}else{
				$this->error(L('edit_error').$user->getDbError());
			}
		}else{
			$this->error($user->getLastSql());
		}
	}
 

	function _before_add(){
	    $m = $this->domain;
		$where = array();
		$where['parentsid'] = array('eq',0);
		$where['rank'] = array('eq',0);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id desc')->select();
		$this->assign('province',json_encode($data));
		unset($where);
		$where['rank'] = array('eq',1);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id desc')->select();
		$this->assign('city',json_encode($data));
		unset($where);
		$where['rank'] = array('eq',2);
		$where['enable'] = array('eq',1);
		$vo['domaininfo_id']=3;
		$vo['rankpath']='1,2';
		$this->assign('vo',$vo);
		$data = $m->where($where)->order('order desc,id desc')->select();
		$this->assign('area',json_encode($data));
	}

	function edit(){
	    $m = $this->domain;
		$where = array();
		$where['parentsid'] = array('eq',0);
		$where['rank'] = array('eq',0);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id desc')->select();
		$this->assign('province',json_encode($data));
		unset($where);
		$where['rank'] = array('eq',1);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id desc')->select();
		$this->assign('city',json_encode($data));
		unset($where);
		$where['rank'] = array('eq',2);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id desc')->select();
		$this->assign('area',json_encode($data));
		
		
		$id = intval($_GET['id']);
		$vo = $this->dao->getByuserinfo_id($id);
		$this->assign('vo',$vo);
		$this->display();
	}
}
?>
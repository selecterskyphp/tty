<?php
/**
 * 
 * User(会员管理文件)
 *
 */
class UserRoleAction extends AdminbaseAction {

    public $dao;
	function _initialize()
	{
		parent::_initialize();
		$this->dao = D('Home.Role');
	}


	function index(){
		$this->assign($_GET);
		$list=$this->dao->order('id desc')->select();
		$this->assign('list',$list);
		$this->display();
	}

    public function edit()
    {
        $id = intval($_GET['id']);
		$vo = $this->dao->getByuserrole_id($id);
		$this->assign('vo',$vo);
		$this->display();
	}
	
	public function update()
	{
	    $model = $this->dao;
	    $where['id'] = intval($_POST['id']);
	    $data['name'] = trim($_POST['name']);
	    
	    $this->dao->where($where)->data($data)->save();
	    $this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
	    $this->success (L('edit_ok'));
	}

	public function insert()
    {
		$model = $this->dao;
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
		$typeid = $model->add() ;
		if ($typeid) {
			//$data['typeid'] = $data['keyid'] = $typeid; 
			$model->save($data);
			$this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
			$this->success (L('add_ok'));
		} else {
			$this->error (L('add_error').': '.$model->getDbError());
		}
	}
}
?>
<?php
if(!defined("TTY")) exit("Access Denied");
class BaseAction extends Action
{
	protected   $Config ,$sysConfig,$categorys,$module,$moduleid,$mod,$dao,$lang;
	protected $isLogin = false;
	protected $login = null;
    public function _initialize() {
		    session_start();
			$this->categorys = F('Category');
			$this->Config = F('Config');
			$this->sysConfig = F('sys.config');
			$this->module = F('Module');
			$this->assign($this->Config);
			$this->mod= F('Mod');
			$this->moduleid=$this->mod[MODULE_NAME];
			$this->assign('Module',$this->module);
			$this->assign('Categorys',$this->categorys);
			$this->assign('module_name',MODULE_NAME);
			$this->assign('action_name',ACTION_NAME);
			$this->lang = L();			
			$this->assign('Lang',$this->lang);
			$this->login = $_SESSION['login'];
			if($this->login)
			{
				$this->isLogin=true;
			}
			$this->assign('login',$this->login);
			$this->assign('isLogin',$this->isLogin);
			//var_dump($this->login,isset($this->login),$this->isLogin);
			//exit;
	}

    public function index($catid='',$module='')
    {
		$name = MODULE_NAME;
		$model = D ('Home.'.$name);
        $list = $model->where($_REQUEST['where'])->select();
		$this->assign($_GET);
        $this->assign('list', $list);
        $this->display();
    }

	public function verify()
	{
		$type	 =	 isset($_GET['type'])?$_GET['type']:'gif';
        import("@.ORG.Image");
        Image::buildImageVerify(4,1,$type,46,20);
	}
	public function login()
	{
		$username = trim($_POST['username']);
        $password = trim($_POST['password']);
        $verifyCode = trim($_POST['verifyCode']);

        if(empty($username) || empty($password)){
            $this->assign('jumpUrl',U('Index/index'));
           	$this->error('请输入正确的用户名和密码');
        }elseif(md5($verifyCode) != $_SESSION['verify']){
           $this->assign('jumpUrl',U('Index/index'));
           $this->error('验证码不正确');
        }
        $m = D('Home.'.'User');
        $where['name']=array('eq',$username);
        $where['passwd']=array('eq',sysmd5($password));
		$r = $m->where($where)->find();
		//var_dump($r,$m->getLastSql());
		//exit;
		if(!$r)
		{
		    $this->assign('jumpUrl',U('Index/index'));
			$this->error('用户名或密码错误');
		}
		else
		{
		    //登录成功 更新登录时间和登录次数
    		$data['lastlogintime'] = $r['logintime'];
    		$data['logintime'] = time();
    		$data['logincount'] = $r['logincount']+1;
    		$where = array();
    		$where['id'] = $r['id'];
    		$m->data($data)->where($where)->save();
		    $this->setLogin($r);
			$this->writeLog($r['name'] . ' 成功登录会员管理中心');
			$this->assign('jumpUrl',U('User/index'));
			$this->success('登录成功');
		}
	}
	
	public function setLogin($r)
	{
	    //获取所在组名
	    //var_dump($r);
	    $mod =D('Home.'.'Role');
	    $where = array();
		$where['id'] = array('eq',$r['userrole_id']);
		$name = $mod->where($where)->getField('name');
		$r['groupname'] = $name;
		$r['groupid'] = $r['userrole_id'];
		//获取所在公司性质名称
		$mod = D('Home.'.'CompanyType');
		$where = array();
		$where['id'] = array('eq',$r['companytype_id']);
		$name = $mod->where($where)->getField('name');
		$r['companytype_name'] = $name;
		//获取省份具体名称
		$r['province_name'] = $this->getProvince($r['domaininfo_id'],$r['rankpath']);
		$_SESSION['login'] = $r;
		//var_dump($_SESSION['login']);
		//exit;
	}

	public function logout()
	{
	    $this->writeLog($this->login['name'] . ' 退出会员管理中心');
		unset($_SESSION['login']);
		$this->assign('jumpUrl',U('Index/index'));
		$this->success('退出成功');
	}
	
	
	/**
	 * 
	 * 写日志
	 * @param unknown_type $msg
	 */
	function writeLog($msg,$type=1)
	{
	    $mod = D('Home.'.'Log');
	    $data = array();
	    $data['userinfo_id'] = $_SESSION['login']['id'];
	    $data['info'] = $msg;
	    $data['type'] = $type;
	    $data['time'] = time();
	    $mod->data($data)->add();
	}
	/**
	 * 
	 * Enter 获取省份名称
	 * @param unknown_type $parent_id
	 * @param unknown_type $rank_path
	 */
	function getProvince($parent_id,$rank_path=null)
	{
	    if(!$parent_id)
	    {
	        return null;
	    }
	    $mod = D('Home.'.'DomainInfo');
	    $where = array();
	    $where['id'] = array('eq',$parent_id);
	    $name = $mod->where($where)->getField('name');
	    $curName = $name;
	    $list = '';
	    if($rank_path)
	    {
	        $tarr = explode(',', $rank_path);
	        foreach ($tarr as $v) 
	        {
	            $where = array();
	            $where['id'] = array('eq',$v);
	            $name = $mod->where($where)->getField('name');
	            $list = ('' == $list)?$name:$list . ' ' . $name;
	        }
	    }
	    if($list != '')
	    {
	        $curName = $list .' ' .$curName;
	    }
	    return $curName;
	}
	/**
	 * 
	 * 获取智能电话/烟感器自检/巡检的状态
	 * @param unknown_type $device_id 设备的ID
	 * @param unknown_type $device_type 'Devices'表示智能电话 Alpha表示烟感器 注意区分大小写
	 * @param unknown_type $status_type 0表示自检 1表示巡检
	 * @param unknown_type $field 要返回的字段
	 */
	function getDeviceStatus($device_id,$device_type='Devices',$status_type=0,$field='result,time')
	{
	    $detct = D('Home.'.$device_type.'Detect');
	    $where['type'] = $status_type;
	    if('Devices' == $device_type)
	    {
	        $where['telecomphoneinfo_id']=$device_id;
	    }
	    else
	    {
	        $where['fogsensorinfo_id']=$device_id;
	    }
	    $result = $detct->where($where)->field($field)->find();
	    if(!$result)
	    {
	        $result['result'] = 2;
	    }
	    return $result;
	}
	
	/**
	 * 
	 * 设置智能电话/烟感器自检/巡检的状态
	 * @param unknown_type $device_id 设备的ID
	 * @param unknown_type $device_type 'Devices'表示智能电话 Alpha表示烟感器 注意区分大小写
	 * @param unknown_type $status_type 0表示自检 1表示巡检
	 * @param unknown_type $field 要返回的字段
	 */
	function setDeviceStatus($device_id,$status,$device_type='Devices',$status_type=0,$sign=0)
	{
	    $detct = D('Home.'.$device_type.'Detect');
	    $where['type'] = $status_type;
	    $filed_id_name = ('Devices' == $device_type)?'telecomphoneinfo_id':'fogsensorinfo_id';
	    $where[$filed_id_name]=$device_id;
	    $result = $detct->where($where)->field('id')->find();
	    if(!$result)
	    {
	        $data['sign']=$sign;//标识需要自检
            $data['type']=$status_type;//0表示自检 1表示巡检
            $data['result']=$status;//检测结果 1表示检测中
            $data[$filed_id_name]=$device_id;//设备ID
            $data['time']=time();//最后一次检测时间
            $detct->data($data)->add();
	    }
	    else 
	    {
	        //$data['sign']=$sign;//标识需要自检
            $data['type']=$status_type;//0表示自检 1表示巡检
            $data['result']=$status;//检测结果 1表示检测中
            //$data['time']=time();//最后一次检测时间
            $detct->where($where)->data($data)->save();
	    }
	    //var_dump($detct->getLastSql());
	    //exit;
	}
}
?>
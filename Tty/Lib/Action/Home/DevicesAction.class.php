﻿<?php
if(!defined("TTY")) exit("Access Denied");
class DevicesAction extends UserbaseAction
{
	public $group,$fireGroup,$user,$dao,$alpha;
    function _initialize()
    {
		$this->group = D("Home.DevicesGroup");
		$this->dao = D('Home.Devices');
		$this->alpha = D('Home.Alpha');
		$this->fireGroup = D('Home.FireGroup');
		$this->user = D("Home.User");
		$this->pagename = '设备信息管理';
		parent::_initialize();
    }
	function index()
	{
	    import ( '@.ORG.Page' );
		$status=isset($_GET['status'])?trim($_GET['status']):'-1';
		$status = (empty($status) || '-1' == $status)?-1:intval($status);
		$keyword = isset($_GET['keyword'])?trim($_GET['keyword']):'';
		$companyname = isset($_GET['companyname'])?trim($_GET['companyname']):'';
		$userid = isset($_GET['userid'])?intval($_GET['userid']):'';
		$starttime = isset($_GET['starttime'])?strtotime($_GET['starttime']):0;
        $endtime = isset($_GET['endtime'])?strtotime($_GET['endtime']):0;
		$where = 'telecomphoneinfo_id>0';
		if($status>=0)
		{
		    //$where .= ' and status='.$status;
		}
		
		if(strlen($keyword)>0)
		{
		    $where .= ' and telecomphoneinfo_number like \'%'.$keyword.'%\'';
		}
	    if($starttime>0)
        {
            $ttime = $starttime - 60*60*24;
            $where .= ' and telecomphoneinfo_createtime>'.$ttime;
        }
        if($endtime>0)
        {
            $ttime = $endtime + 60*60*24;
            $where .= ' and telecomphoneinfo_createtime<'.$ttime;
        }
		if(3 == $this->login['groupid'])
		{
		    //如果是商户，只显示属于商户的设备
		    $where .= ' and userinfo_id='.$this->login['id'];
		    $detect_type = 0;//标识展示设备状太为自检后的状态
		}
		elseif(2 == $this->login['groupid']) 
		{
		    //如果是消防用户，则显示该消防用户区域管辖下的设备
    		if(!empty($userid))
    		{
    		    //如果有过滤商户
    		    $where .= ' and userinfo_id='.$userid;
    		}
    		if(strlen($companyname)>1)
    		{
    		    $where .= ' and userinfo_id in (select userinfo_id from '.C('DB_PREFIX').'user_info where domaininfo_id='.$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\' and userinfo_companyname like \'%'.$companyname.'%\')';
    		}
    		else 
    		{
    		    $where .= ' and userinfo_id in (select userinfo_id from '.C('DB_PREFIX').'user_info where domaininfo_id='.$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\')';
    		}
		    $detect_type = 1;//标识展示设备状太为巡检后的状态
		}
		$this->assign($_GET);
		$count=$this->dao->where($where)->count();
		$page=new Page($count);
		$show=$page->show();
		//var_dump($count,$show);
		$this->assign("page",$show);
		$list=$this->dao->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($this->dao->getLastSql());
		//exit;
		$tmp = array();
		$detctG = D('Home.DevicesDetect');
		foreach($list as $v)
		{
			$name = $this->group->where("firecallergroup_id=".$v['firecallergroup_id'])->getField("name");
			$v['groupname']=$name;
			//获取设备的状态
			$status=C('DEVICES_STATUS');
			$result = $detctG->where('telecomphonedetect_type=' .$detect_type.' and telecomphoneinfo_id='.$v['id'])->field('result,time')->find();
			//var_dump($result);
			if(null === $result || false === $result)
			{
			    $result['result'] = 2;//未知
			    $v['statustime'] = 0;
			}
			else
			{
			    $v['statustime']=$result['time'];
			}
			$v['statusname']=$status[$result['result']];
			//var_dump($result,$v);
			//消防组
			$name = $this->fireGroup->where("firecontrollergroup_id=".$v['firecontrollergroup_id'])->getField("name");
			$v['firegroupname']=$name;
			//所属商户
			$name = $this->user->where("userinfo_id=".$v['userinfo_id'])->getField("name");
			$v['username']=$name;
			
			$tmp[]=$v;
		}
		
		
		$this->assign('list',$tmp);
		$alpha = null;
		if($list)
		{
			$alpha = $this->alpha->where('telecomphoneinfo_id='.$list[0]['id'])->select();
			
		}
		$altmp = array();
		$detctA = D('Home.AlphaDetect');
		foreach ($alpha as $v)
		{
			$status=C('DEVICES_STATUS');
			$result = $detctA->where('fogsensordetect_type=' .$detect_type.' and fogsensorinfo_id='.$v['id'])->getField('result');
			
			if(null === $result || false === $result)
			{
			    $result = 2;//未知
			}
			$v['statusname']=$status[$result];
			$tmp = $time - $v['fogsensorcell_createtime'];
			$tmp = ceil($tmp / 86400);//电池安装到现在经过的天数
			$tmp = C('BATTERY_VALID') - $tmp;//算出电池剩余天数
			if($tmp < 0)
			{
			    $tmp = 0;
			}
			$v['days'] = $tmp;//电池安装到现在的时间间隔
			$v['createtime'] = date('Y/m/d H:i:s',$v['createtime']);
			$v['fogsensorcell_createtime'] = date('Y/m/d H:i:s',$v['fogsensorcell_createtime']);
			$altmp[]=$v;
		}
		if(0 == $starttime)
		{
		    $starttime = strtotime(date('Y-m',time()).'-01');
		}
		if(0 == $endtime)
		{
		    $endtime = time();
		}
		$this->assign('starttime',date('Y-m-d',$starttime));
		$this->assign('endtime',date('Y-m-d',$endtime));
		$status_list = C('DEVICES_STATUS');
		$this->assign('status_list',$status_list);
		
		$this->assign('alpha',$altmp);
		$this->assign('statuslist',C('DEVICES_STATUS'));
		$userlist = $this->user->where('domaininfo_id='.$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\' and userrole_id=3')->field('id,name')->select();
		//var_dump($this->user->getLastSql());
		$this->assign('userlist',$userlist);
		$this->display();
	}
	
	function getAlpha()
	{
		$id = isset($_GET['id'])?intval($_GET['id']):0;
		$list = $this->alpha->where('telecomphoneinfo_id='.$id)->select();
		$tmpArr = array();
		$detctA = D('Home.AlphaDetect');
	    if(3 == $this->login['groupid'])
		{
		    //如果是商户，只显示属于商户的设备
		    $detect_type = 0;//标识展示设备状太为自检后的状态
		}
		elseif(2 == $this->login['groupid']) 
		{
		    //如果是消防用户，则显示该消防用户区域管辖下的设备
		    $detect_type = 1;//标识展示设备状太为巡检后的状态
		}
		foreach ($list as $v) {
			$status=C('DEVICES_STATUS');
			$result = $detctA->where('fogsensordetect_type=' .$detect_type.' and fogsensorinfo_id='.$v['id'])->getField('result');
			if(null === $result || false === $result)
			{
			    $result = 2;//未知
			}
			$v['statusname']=$status[$result];
			$t = $time - $v['fogsensorcell_createtime'];
			$tmp = ceil($tmp / 86400);//电池安装到现在经过的天数
			$tmp = C('BATTERY_VALID') - $tmp;//算出电池剩余天数
			if($tmp < 0)
			{
			    $tmp = 0;
			}
			$v['days'] = $tmp;//电池安装到现在的时间间隔
			$v['createtime'] = date('Y/m/d H:i:s',$v['createtime']);
			$v['fogsensorcell_createtime'] = date('Y/m/d H:i:s',$v['fogsensorcell_createtime']);
			$tmpArr[]=$v;
		}
		echo json_encode($tmpArr);
		exit;
	}
	function check()
	{
	    //将状态设为要求检测 目前只有商户有这个功能
	    $id = isset($_GET['id'])?intval($_GET['id']):'';
	    $type = isset($_GET['type'])?intval($_GET['type']):'';
	    $data = array();
	    if(0 == $type)
	    {
	        //如果是电话机
	        $mod = D('Home.DevicesDetect');
	        $where = 'telecomphonedetect_type=0 and telecomphoneinfo_id='.$id;
	        $result = $mod->where($where)->getField('telecomphoneinfo_id');
	        if(!$result)
	        {
	            $data['telecomphonedetect_sign']=1;//标识需要自检
	            $data['telecomphonedetect_type']=0;//0表示自检 1表示巡检
	            $data['telecomphonedetect_result']=1;//检测结果 1表示检测中
	            $data['telecomphoneinfo_id']=$id;//设备ID
	            $data['telecomphonedetect_time']=time();//最后一次检测时间
	            $mod->data($data)->add();
	        }
	        else 
	        {
	            $data['telecomphonedetect_sign']=1;
	            $data['telecomphonedetect_result']=1;
	            $data['telecomphonedetect_time']=time();
	            $mod->where($where)->data($data)->save();
	        }
	        //写入日志
	        $name = $this->getDevicesInfo($id);
	        $this->writeLog('商户 '.$this->login['name'].' 对智能电话 <b>'.$name.'</b> 进行了自检',2);
	    }
	    else
	    {
	        //如果是烟感器
	        $mod = D('Home.AlphaDetect');
    	    if(3 == $this->login['groupid'])
    		{
    		    //如果是商户，只显示属于商户的设备
    		    $detect_type = 0;//标识展示设备状太为自检后的状态
    		}
    		elseif(2 == $this->login['groupid']) 
    		{
    		    //如果是消防用户，则显示该消防用户区域管辖下的设备
    		    $detect_type = 1;//标识展示设备状太为巡检后的状态
    		}
	        $where = 'fogsensordetect_type='.$detect_type.' and fogsensorinfo_id='.$id;
	        $result = $mod->where($where)->getField('fogsensorinfo_id');
	        if(!$result)
	        {
	            $data['fogsensordetect_sign']=1;//标识需要自检
	            $data['fogsensordetect_type']=$detect_type;//0表示自检 1表示巡检
	            $data['fogsensordetect_result']=1;//检测结果 1表示检测中
	            $data['fogsensorinfo_id']=$id;//设备ID
	            $data['fogsensordetect_time']=time();//最后一次检测时间
	            $mod->data($data)->add();
	        }
	        else 
	        {
	            $data['fogsensordetect_sign']=1;
	            $data['fogsensordetect_result']=1;
	            $data['fogsensordetect_time']=time();
	            $mod->where($where)->data($data)->save();
	            //var_dump($mod->getLastSql());
	            //exit;
	        }
	        //写入日志
	        $arr = $this->getAlphaInfo($id);
	        $this->writeLog('商户 '.$this->login['name'].' 对智能电话 <b>'.$arr['devices_name'].'</b> 下的烟感器 <b>'.$arr['name'].'</b> 进行了自检',2);
	    }
	    die('1');
	}
	function battery()
	{
	    $id = intval($_GET['id']);
	    $this->alpha->where('fogsensorinfo_id ='.$id)->data('fogsensorcell_createtime='.time())->save();
	    //var_dump($this->alpha->getLastSql());
	    //exit;
	    $arr = $this->getAlphaInfo($id);
	    $this->writeLog('商户 '.$this->login['name'] .' 对智能电话 <b>'.$arr['devices_name'].'</b> 下的烟感器 <b>'.$arr['name'].'</b> 更换电池成功',2);
	    $this->assign('jumpUrl',U('Devices/index'));
	    $this->success('更换电池成功');
	}
	function edit()
	{
	    $id = intval($_GET['id']);
	    $where['id']=$id;
	    $vo = $this->dao->where($where)->find();
	    $this->assign('vo',$vo);
	    $status_list=C('DEVICES_STATUS');
	    $status_result = $this->getDeviceStatus($id);
	    $status = $status_result['result'];
	    $this->assign('status_list',$status_list);
	    $this->assign('status',$status);
	    $this->display();
	}
	function update()
	{
	    $id = intval($_POST['id']);
	    $status = intval($_POST['__status']);
	    $this->setDeviceStatus($id, $status);
	    $this->assign('jumpUrl',U('Devices/index'));
	    parent::update();
	}
	function alphaedit()
	{
	    $id = intval($_GET['id']);
	    $where['id']=$id;
	    $vo = $this->alpha->where($where)->find();
	    $this->assign('vo',$vo);
	    $status_list=C('DEVICES_STATUS');
	    $status_result = $this->getDeviceStatus($id,'Alpha');
	    $status = $status_result['result'];
	    $this->assign('status_list',$status_list);
	    $this->assign('status',$status);
	    $this->display();
	}
	function alphaupdate()
	{
	    $id = intval($_POST['id']);
	    $status = intval($_POST['__status']);
	    $this->setDeviceStatus($id, $status,'Alpha');
	    $where['id'] = $id;
	    $data['location'] = trim($_POST['location']);
	    $this->alpha->where($where)->data($data)->save();
	    $this->assign('jumpUrl',U('Devices/index'));
	    $this->success ('保存成功');
	}
}
?>
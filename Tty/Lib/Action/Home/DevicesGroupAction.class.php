<?php
if(!defined("TTY")) exit("Access Denied");
class DevicesGroupAction extends UserbaseAction
{
    public $dao;
    function _initialize()
    {
		$this->dao = D('Home.'.MODULE_NAME);
		$this->pagename = '群呼电话组信息';
		parent::_initialize();
    }
    function index()
    {
        import ( '@.ORG.Page' );
        $where = 'userinfo_id = '.$this->login['id'];
        $count=$this->dao->where($where)->count();
		$page=new Page($count,2);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$this->dao->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$tmp = array();
		$mod = D("Home.DevicesPeople");
		$where = array();
		$where['ismain']=1;
		$where['userinfo_id']=$this->login['id'];
		foreach ($list as $v)
		{
		    $where['firecallergroup_id']=$v['id'];
		    $main = $mod->where($where)->find();
		    if(!$main)
		    {
		        $main['number']='暂无主叫电话';
		    }
		    $v['main']=$main;
		    $tmp[] = $v;
		}
		$this->assign('list',$tmp);
		$this->display();
    }
}
?>
<?php
if(!defined("TTY")) exit("Access Denied");
class DevicesPeopleAction extends UserbaseAction
{
    public $dao,$gid;
    function _initialize()
    {
		$this->dao = D('Home.'.MODULE_NAME);
		$this->gid=$gid = isset($_GET['gid'])?intval($_GET['gid']):0;
		$mod = D('Home.DevicesGroup');
		$where['id']=$gid;
		$name = $mod->where($where)->getField('name');
		$this->assign('groupname',$name);
		$this->assign('gid',$gid);
		$this->pagename = '群呼电话组信息';
		parent::_initialize();
    }
    function index()
    {
        import ( '@.ORG.Page' );
        $where = 'userinfo_id = '.$this->login['id'].' and firecallergroup_id='.$this->gid;
        $count=$this->dao->where($where)->count();
		$page=new Page($count,15);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$this->dao->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$this->assign('list',$list);
		$this->display();
    }
    
    function setmain()
	{
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $mod = $this->dao;
	    $data['ismain']=0;
	    $where['firecallergroup_id']=$this->gid;
	    $where['userinfo_id']=$this->login['id'];
	    $mod->where($where)->data($data)->save();
	    //var_dump($mod->getLastSql());
	    unset($where);
	    $data['ismain']=1;
	    $where['id']=$id;
	    $where['firecallergroup_id']=$this->gid;
	    $where['userinfo_id']=$this->login['id'];
	    $mod->where($where)->data($data)->save();
	    //var_dump($mod->getLastSql());
	    //exit;
	    $this->redirect(U('DevicesPeople/index',array('gid'=>$this->gid)));
	}
    function insert()
	{
		$model = $this->dao;
	    $where['ismain'] = 1;
	    $where['userinfo_id'] = $this->login['id'];
	    $where['firecallergroup_id'] = $this->gid;
	    $result = $model->where($where)->getField('id');
	    if(!$result)
	    {
	        //如果没有设置主号码，则自动将自动设置为主号码
	        $_POST['ismain']=1;
	    }
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
		if ($model->add() !==false) {
		    
			$this->assign ( 'jumpUrl', U(MODULE_NAME.'/index',array('gid'=>$this->gid)) );
			$this->success (L('add_ok'));
		} else {
			$this->error (L('add_error').': '.$model->getDbError());
		}
	}
}
?>
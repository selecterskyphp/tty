<?php
if(!defined("TTY")) exit("Access Denied");
class DownloadAction extends UserbaseAction
{
    protected $dao,$group,$alpha,$fireGroup,$user;
    public function index($catid='',$module='')
    {
        $this->dao = D('Home.Devices');
        $this->group = D("Home.DevicesGroup");
        $this->alpha = D('Home.Alpha');
        $this->fireGroup = D('Home.FireGroup');
		$this->user = D("Home.User");
		$file = 'tmp.csv';
        $filename = CACHE_PATH . '/' .$file;
        $fp = fopen($filename, 'w');
        $status=isset($_GET['status'])?intval($_GET['status']):'';
		$keyword = isset($_GET['keyword'])?trim($_GET['keyword']):'';
		$userid = isset($_GET['userid'])?intval($_GET['userid']):'';
		$where = 'telecomphoneinfo_id>0';
		if(!empty($status))
		{
		    $where .= ' and status='.$status;
		}
		
		if(strlen($keyword)>0)
		{
		    $where .= ' and telecomphoneinfo_number like \'%'.$keyword.'%\'';
		}
		if(3 == $this->login['groupid'])
		{
		    //如果是商户，只显示属于商户的设备
		    $where .= ' and userinfo_id='.$this->login['id'];
		    $detect_type = 0;//标识展示设备状太为自检后的状态
		}
		elseif(2 == $this->login['groupid']) 
		{
		    //如果是消防用户，则显示该消防用户区域管辖下的设备
    		if(!empty($userid))
    		{
    		    //如果有过滤商户
    		    $where .= ' and userinfo_id='.$userid;
    		}
		    $where .= ' and userinfo_id in (select userinfo_id from '.C('DB_PREFIX').'user_info where domaininfo_id='.$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\')';
		    $detect_type = 1;//标识展示设备状太为巡检后的状态
		}
		$list=$this->dao->order('id desc')->where($where)->select();
		//var_dump($this->dao->getLastSql());
		//exit;
		$detctG = D('Home.DevicesDetect');
		$detctA = D('Home.AlphaDetect');
		$col = array('智能电话名称','智能电话主叫号码','智能电话安装时间','安装位置');
		fputcsv($fp, $col);
		foreach($list as $v)
		{
			$name = $this->group->where("firecallergroup_id=".$v['firecallergroup_id'])->getField("name");
			$v['groupname']=$name;
			//获取设备的状态
			$status=C('DEVICES_STATUS');
			$result = $detctG->where('telecomphonedetect_type=' .$detect_type.' and telecomphoneinfo_id='.$v['id'])->field('result,time')->find();
			//var_dump($result);
			if(!$result)
			{
			    $result['result'] = 2;//未知
			    $v['statustime'] = 0;
			}
			else
			{
			    $v['statustime']=$result['time'];
			}
			$v['statusname']=$status[$result['result']];
			//var_dump($result,$v);
			//消防组
			$name = $this->fireGroup->where("firecontrollergroup_id=".$v['firecontrollergroup_id'])->getField("name");
			$v['firegroupname']=$name;
			//所属商户
			$name = $this->user->where("userinfo_id=".$v['userinfo_id'])->getField("name");
			$v['username']=$name;
			$dao = $v;
			unset($dao['id'],$dao['userinfo_id'],$dao['firecallergroup_id'],$dao['firecontrollergroup_id']);
			$dao['createtime']=date('Y-m-d H:i:s',$dao['createtime']);
			fputcsv($fp, $dao);
			//输出烟感器的标题
			//$col = array('============','','');
			//fputcsv($fp, $col);
			//$col = array('烟感器名称','烟感器安装位置','烟感器安装时间');
			//fputcsv($fp, $col);
    		foreach ($alpha as $vv)
    		$alpha = $this->alpha->where('telecomphoneinfo_id='.$v['id'])->select();
    		{
    			$status=C('DEVICES_STATUS');
    			$result = $detctA->where('fogsensordetect_type=' .$detect_type.' and fogsensorinfo_id='.$vv['id'])->getField('result');
    			//var_dump($result);
    			if(!$result)
    			{
    			    $result = 2;//未知
    			}
    			$vv['statusname']=$status[$result];
    			$vv['createtime'] = date('Y/m/d H:i:s',$vv['createtime']);
    			unset($dao);
    			$dao = $vv;
    			unset($dao['id'],$dao['telecomphoneinfo_id'],$dao['userinfo_id']);
    			$dao['createtime']=date('Y-m-d H:i:s',$dao['createtime']);
    			fputcsv($fp, $dao);
    		}
		}
		fclose($fp);
		$content = file_get_contents($filename);
		@unlink($filename);
		$content = mb_convert_encoding($content,'gb2312','utf-8');
        import("@.ORG.Http");
        Http::download('','tmp.csv',$content);
    }
}
?>
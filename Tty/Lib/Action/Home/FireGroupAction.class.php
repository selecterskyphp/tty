<?php
if(!defined("TTY")) exit("Access Denied");
class FireGroupAction extends UserbaseAction
{
    public $dao;
    function _initialize()
    {
		$this->dao = D('Home.'.MODULE_NAME);
		$this->pagename = '消防责任组信息';
		parent::_initialize();
    }
    function index()
    {
        import ( '@.ORG.Page' );
        $where = 'userinfo_id = '.$this->login['id'];
        $count=$this->dao->where($where)->count();
		$page=new Page($count,15);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$this->dao->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		$tmp = array();
		$mod = D("Home.FirePeople");
		$where = array();
		$where['ismain']=1;
		$where['userinfo_id']=$this->login['id'];
		foreach ($list as $v)
		{
		    $where['firecontrollergroup_id']=$v['id'];
		    $main = $mod->where($where)->find();
		    if(!$main)
		    {
		        $main['name']='暂无消防员';
		    }
		    $v['main']=$main;
		    $tmp[] = $v;
		}
		$this->assign('list',$tmp);
		$this->assign('userid',$this->login['id']);
		$this->display();
    }
}
?>
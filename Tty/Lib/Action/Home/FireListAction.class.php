<?php
if(!defined("TTY")) exit("Access Denied");
class FireListAction extends UserbaseAction
{
    public $dao;
    function _initialize()
    {
		$this->dao = D('Home.'.MODULE_NAME);
		$this->pagename = '报警查询';
		parent::_initialize();
    }
    function index($type = 0) 
    {
        import ( '@.ORG.Page' );
        if(3 == $this->login['groupid'])
        {
            //商户
            $where = 'userinfo_id = '.$this->login['id'];
        }
        else 
        {
            //消防
            $where = 'userinfo_id in(select userinfo_id from '.C('DB_PREFIX').'user_info where domaininfo_id = '.$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\'  and userrole_id=3)';
        }
        $mod = D('Home.Devices');
        $devices_list = $mod->where($where)->select();
        $this->assign('devices_list',$devices_list);
        $starttime = isset($_GET['starttime'])?strtotime($_GET['starttime']):0;
        $endtime = isset($_GET['endtime'])?strtotime($_GET['endtime']):0;
        $fire_type = isset($_GET['fire_type'])?intval($_GET['fire_type']):-1;
        $devices = isset($_GET['devices'])?intval($_GET['devices']):0;
        $keyword = isset($_GET['keyword'])?trim($_GET['keyword']):'';
        $companyname = isset($_GET['companyname'])?trim($_GET['companyname']):'';
        if($starttime>0)
        {
            $tmptime = $starttime - 60*60*24;
            $where .= ' and firerecordinfo_time>'.$tmptime;
        }
        if($endtime>0)
        {
            $tmptime = $endtime + 60*60*24;
            $where .= ' and firerecordinfo_time<'.$tmptime;
        }
        if(-1 != $fire_type && 0 != $fire_type)
        {
            $where .= ' and firerecordinfo_type='.$fire_type;
        }
        if($devices>0)
        {
            $where .= ' and telecomphoneinfo_id='.$devices;
        }
        if(strlen($keyword)>0)
        {
            $where .= ' and telecomphoneinfo_number like \'%'.$keyword.'%\'';
        }
        if(strlen($companyname)>0)
        {
            $where .= ' and userinfo_companyname like \'%'.$companyname.'%\'';
        }
        $count=$this->dao->where($where)->count();
		$page=new Page($count,15);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$this->dao->order('id')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($this->dao->getLastSql());
		$tmp = array();
		$status = C('FIRE_STATUS');
		$where=array();
		foreach ($list as $v)
		{
		    //$where['telecomphoneinfo_id']=$v['telecomphoneinfo_id'];
		    //$number = $mod->where($where)->getField('number');
		   // var_dump($mod->getLastSql());
		    //$v['number']=$number;
		    $v['statusname']=$status[$v['type']];
		    $tmp[] = $v;
		}
		if(0 == $starttime)
		{
		    $starttime = strtotime(date('Y-m',time()).'-01');
		}
		if(0 == $endtime)
		{
		    $endtime = time();
		}
		$this->assign('starttime',date('Y-m-d',$starttime));
		$this->assign('endtime',date('Y-m-d',$endtime));
		$this->assign('keyword',$keyword);
		$this->assign('companyname',$companyname);
		$fire_type_list = C('FIRE_STATUS');
		$this->assign('fire_type_list',$fire_type_list);
		$this->assign('fire_type',$fire_type);
		$this->assign('devices',$devices);
		$this->assign('list',$tmp);
		if(0 == $type)
		{
		    $this->display();
		}
		else 
		{
		    return $tmp;
		}
    }
    public function export()
	{
	    $data = $this->index(1);
	    $col = array('告警时间','告警类型','备注','主叫号码','类型说明');
	    $this->export_data($data,$col);
	}
}
?>
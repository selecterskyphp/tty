<?php
if(!defined("TTY")) exit("Access Denied");
class GuestbookAction extends UserbaseAction
{

    protected $dao;
    function _initialize()
    {
        $this->pagename = '邮件列表';
        $this->dao= M(MODULE_NAME);
        parent::_initialize();
    }
    public function index()
    {
		//var_dump($this->dao);
		$count = $this->dao->where($where)->count();
		if($count){
			import ( "@.ORG.Page" );
			$listRows =  !empty($cat['pagesize']) ? $cat['pagesize'] : C('PAGE_LISTROWS');		
			$page = new Page ( $count, $listRows );
			$pages = $page->show();
			$where['receive_userid']=$this->login['id'];
			$list = $this->dao->where($where)->order('id desc')->limit($page->firstRow . ',' . $page->listRows)->select();
			$this->assign('pages',$pages);
			$this->assign('list',$list);
		}
        $this->display();
    }

	public function _before_insert(){
		$_POST['ip'] = get_client_ip();
	}
    
	public function show()
	{
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $where['id']=$id;
	    $where['receive_userid']=$this->login['id'];
	    $data = $this->dao->where($where)->find();
	    //var_dump($this->dao->getLastSql());
	    $this->assign('v',$data);
	    $this->display();
	}
    public function replay()
	{
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $where['id']=$id;
	    $where['receive_userid']=$this->login['id'];
	    $data = $this->dao->where($where)->find();
	    $this->assign('v',$data);
	    $this->display();
	}
	public function replaysave()
	{
	    $id = isset($_POST['id'])?intval($_POST['id']):0;
	    $where['id']=$id;
	    $where['receive_userid']=$this->login['id'];
	    $vo = $this->dao->where($where)->find();
	    $this->dao->where($where)->data('status=1')->save();
	    unset($_POST['id']);
	    $_POST['receive_userid'] = $vo['send_userid'];
	    $_POST['createtime'] = time();
	    $_POST['send_userid'] = $this->login['id'];
	    $_POST['send_name'] = $this->login['name'];
	    $_POST['status'] = 0;
	    if($this->dao->create())
	    {
	        if($this->dao->add())
	        {
	            //$this->assign('jumpUrl',U("Guestbook/index"));
	            $this->success('邮件回复成功');
	        }
	        else
	        {
	            //var_dump($this->dao->getLastSql());
	            $this->error('邮件回复失败');
	        }
	    }
	    else
	    {
	        $this->error('操作异常');
	    }
	    $this->assign('id',$id);
	    $this->display();
	}
    public function send()
	{
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $this->assign('id',$id);
	    $this->display();
	}
	public function mailsend()
	{
	    $this->pagename = '邮件发送';
	    $this->assign('pagename',$this->pagename);
	    $this->display();
	}
	public function sendsave()
	{
	    $id = isset($_POST['id'])?intval($_POST['id']):0;
	    $name = isset($_POST['user_name'])?trim($_POST['user_name']):'';
	    $mod = D('Home.User');
	    if(strlen($name)>0)
	    {
	        $where['name']=$name;
	        $id = $mod->where($where)->getField('id');
	        if(!$id)
	        {
	            $this->error('用户不存在');
	        }
	    }
	    else
	    {
	        $where['id']=$id;
	        $name = $mod->where($where)->getField('name');
	        if(!$name)
	        {
	            $this->error('用户不存在');
	        }
	    }
	    unset($_POST['user_name'],$_POST['id']);
	    $_POST['receive_userid'] = $id;
	    $_POST['createtime'] = time();
	    $_POST['send_userid'] = $this->login['id'];
	    $_POST['send_name'] = $this->login['name'];
	    $_POST['status'] = 0;
	    if($this->dao->create())
	    {
	        if($this->dao->add())
	        {
	            //$this->assign('jumpUrl',U("Guestbook/mailsend"));
	            $this->success('邮件发送成功');
	        }
	        else
	        {
	            $this->error('邮件发送失败');
	        }
	    }
	    else
	    {
	        $this->error('操作异常');
	    }
	}
}
?>
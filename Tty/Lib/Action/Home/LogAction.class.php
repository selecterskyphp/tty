<?php
if(!defined("TTY")) exit("Access Denied");
class LogAction extends UserbaseAction
{
    public $dao;
    function _initialize()
    {
		$this->dao = D("Home.".MODULE_NAME);
		$this->pagename = '日志查询';
		parent::_initialize();
    }
    function index($type=0)
    {
        import ( '@.ORG.Page' );
        if(3 == $this->login['groupid'])
        {
            $where = 'userinfo_id = '.$this->login['id'];
        }
        else 
        {
            $where .= ' (userinfo_id = '.$this->login['id'] .' or  userinfo_id in (select userinfo_id from '.C('DB_PREFIX').'user_info where domaininfo_id='.$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\'))';
        }
        $starttime = isset($_GET['starttime'])?strtotime($_GET['starttime']):0;
        $endtime = isset($_GET['endtime'])?strtotime($_GET['endtime']):0;
        $f_type = isset($_GET['f_type'])?intval($_GET['f_type']):0;
        if($starttime>0)
        {
            $tmpTime = $starttime - 60*60*24;
            $where .= ' and log_time>='.$tmpTime;
        }
        if($endtime>0)
        {
            $tmpTime = $endtime + 60*60*24;
            $where .= ' and log_time<='.$tmpTime;
        }
        if($f_type>0)
        {
            $where .= ' and log_type='.$f_type;
        }
        $count=$this->dao->where($where)->count();
		$page=new Page($count,15);
		$show=$page->show();
		$this->assign("page",$show);
		$list=$this->dao->order('id desc')->where($where)
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($this->dao->getLastSql());
		//exit;
		$this->assign('list',$list);
		$this->assign('userid',$this->login['id']);
		if(0 == $starttime)
		{
		    $starttime = strtotime(date('Y-m',time()).'-01');
		}
		if(0 == $endtime)
		{
		    $endtime = time();
		}
		$this->assign('starttime',date('Y-m-d',$starttime));
		$this->assign('endtime',date('Y-m-d',$endtime));
		$this->assign('f_type',$f_type);
		if(0 == $type)
		{
		    $this->display();
		}
		else 
		{
		    return $list;
		}
    }
    public function export()
	{
	    $data = $this->index(1);
	    $col = array('日志描述','操作时间');
	    $this->export_data($data,$col);
	}
}
?>
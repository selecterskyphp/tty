<?php
if(!defined("TTY")) exit("Access Denied");
class TongjiAction extends UserbaseAction
{
    
	function user()
	{
	    $this->pagename='商户统计';
	    $this->assign('pagename',$this->pagename);
	    $mod = D('Home.User');
	    $year = isset($_GET['year'])?intval($_GET['year']):date('Y',time());
	    $i=0;
	    $data = array();
	    //获取所有月份的统计数据
	    for($i=0;$i<12;$i++)
	    {
	        $where = 'year(from_unixtime(userinfo_createtime ))='.$year.' and month( from_unixtime(userinfo_createtime ))='.($i+1);
	        $data[$i]= $mod->where($where)->count();
	        $data[$i] = intval($data[$i]);
	    }
	    $list = array(
    	    'title'=>array(
    	        'text'=>$year.'年新开户商户统计信息',
    	        "style"=>"{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
    	    ),
    	    'y_legend'=>array(
    	        'text'=>' ',
    	        "style"=> "{color: #736AFF; font-size: 12px;}"
    	    ),
    	    'elements'=>array(
    	        array(
    	        'type'=>'bar_glass',
    	        'alpha'=>0.5,
    	        'colour'=>'#9933CC',
    	        'text'=>'商户',
    	        'font-size'=>10,
    	        'on-show'=>array('type'=>'grow-up'),
    	        'values'=>$data
    	        )
    	    ),
    	    'x_axis'=>array(
    	    'stroke'=>1,
    	    'tick_height'=>10,
    	    'colour'=>'#d000d0',
    	    'grid_colour'=>'#00ff00',
    	    //'labels'=>array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月')
    	    'labels'=>array(
    	        'labels'=>array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月')
    	        )
    	    ),
    	    'y_axis'=>array(
    	    'stroke'=>4,
    	    'tick_length'=>3,
    	    'colour'=>'#d000d0',
    	    'grid_colour'=>'#00ff00',
    	    'offset'=>0,
    	    'max'=>20
    	    )
	    );
	    $this->assign('data',json_encode($list));
	    $this->assign('year',$year);
	    if(isset($_GET['print']))
	    {
	        $this->display(MODULE_NAME . '_' . ACTION_NAME .'_print');
	    }
	    else 
	    {
		    $this->display();
	    }
	}
	function devices()
	{
	    $this->pagename='智能电话统计';
	    $this->assign('pagename',$this->pagename);
	    $mod = D('Home.Devices');
	    $year = isset($_GET['year'])?intval($_GET['year']):date('Y',time());
	    $i=0;
	    $data = array();
	    //获取所有月份的统计数据
	    for($i=0;$i<12;$i++)
	    {
	        $where = 'year(from_unixtime(telecomphoneinfo_createtime ))='.$year.' and month( from_unixtime(telecomphoneinfo_createtime ))='.($i+1);
	        $data[$i]= $mod->where($where)->count();
	        $data[$i] = intval($data[$i]);
	    }
	    $list = array(
    	    'title'=>array(
    	        'text'=>$year.'年新增电信智能电话统计信息',
    	        "style"=>"{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
    	    ),
    	    'y_legend'=>array(
    	        'text'=>' ',
    	        "style"=> "{color: #736AFF; font-size: 12px;}"
    	    ),
    	    'elements'=>array(
    	        array(
    	        'type'=>'bar_glass',
    	        'alpha'=>0.5,
    	        'colour'=>'#9933CC',
    	        'text'=>'智能电话',
    	        'font-size'=>10,
    	        'on-show'=>array('type'=>'grow-up'),
    	        'values'=>$data
    	        )
    	    ),
    	    'x_axis'=>array(
    	    'stroke'=>1,
    	    'tick_height'=>10,
    	    'colour'=>'#d000d0',
    	    'grid_colour'=>'#00ff00',
    	    //'labels'=>array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月')
    	    'labels'=>array(
    	        'labels'=>array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月')
    	        )
    	    ),
    	    'y_axis'=>array(
    	    'stroke'=>4,
    	    'tick_length'=>3,
    	    'colour'=>'#d000d0',
    	    'grid_colour'=>'#00ff00',
    	    'offset'=>0,
    	    'max'=>20
    	    )
	    );
	    $this->assign('data',json_encode($list));
	    $this->assign('year',$year);
	    if(isset($_GET['print']))
	    {
	        $this->display(MODULE_NAME . '_' . ACTION_NAME .'_print');
	    }
	    else 
	    {
		    $this->display();
	    }
	}
	function alarm()
	{
	    $this->pagename='报警统计';
	    $this->assign('pagename',$this->pagename);
	    $mod = D('Home.FireList');
	    $type= isset($_GET['type'])?intval($_GET['type']):0;
	    $year = isset($_GET['year'])?intval($_GET['year']):date('Y',time());
	    $month = isset($_GET['month'])?intval($_GET['month']):date('m',time());
	    //type 0按年统计 1按月统计
	    if(0 == $type)
	    {
    	    $i=0;
    	    $data = array();
    	    //获取所有月份的统计数据
    	    for($i=0;$i<12;$i++)
    	    {
    	        $where = 'year(from_unixtime(firerecordinfo_time  ))='.$year.' and month( from_unixtime(firerecordinfo_time  ))='.($i+1);
    	        $data[$i]= $mod->where($where)->count();
    	        $data[$i] = intval($data[$i]);
    	    }
    	    $list = array(
        	    'title'=>array(
        	        'text'=>$year.'新增报警统计报表',
        	        "style"=>"{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
        	    ),
        	    'y_legend'=>array(
        	        'text'=>' ',
        	        "style"=> "{color: #736AFF; font-size: 12px;}"
        	    ),
        	    'elements'=>array(
        	        array(
        	        'type'=>'line',
        	        'alpha'=>0.5,
        	        'colour'=>'#9933CC',
        	        'text'=>'告警',
        	        'font-size'=>10,
        	        'on-show'=>array('type'=>'grow-up'),
        	        'values'=>$data
        	        )
        	    ),
        	    'x_axis'=>array(
        	    'stroke'=>1,
        	    'tick_height'=>10,
        	    'colour'=>'#d000d0',
        	    'grid_colour'=>'#00ff00',
        	    //'labels'=>array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月')
        	    'labels'=>array(
        	        'labels'=>array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月')
        	        )
        	    ),
        	    'y_axis'=>array(
        	    'stroke'=>4,
        	    'tick_length'=>3,
        	    'colour'=>'#d000d0',
        	    'grid_colour'=>'#00ff00',
        	    'offset'=>0,
        	    'max'=>20
        	    )
    	    );
	    }
	    else 
	    {
	        //火警
	        $data = array();
	        $where = 'firerecordinfo_type=1 and year(from_unixtime(firerecordinfo_time))='.$year.' and month( from_unixtime(firerecordinfo_time  ))='.$month;
    	    $count1 = $mod->where($where)->count();
    	    //其它
    	    $where = 'firerecordinfo_type=2 and year(from_unixtime(firerecordinfo_time))='.$year.' and month( from_unixtime(firerecordinfo_time  ))='.$month;
    	    $count2 = $mod->where($where)->count();
    	    //误报
    	    $where = 'firerecordinfo_type=3 and year(from_unixtime(firerecordinfo_time))='.$year.' and month( from_unixtime(firerecordinfo_time  ))='.$month;
    	    $count3 = $mod->where($where)->count();
    	    $count = $count1 + $count2 + $count3;
    	    $parent1 = ceil(($count1 / $count)*100);
    	    $parent2 = ceil(($count2 / $count)*100);
    	    $parent3 = ceil(($count3 / $count)*100);
	        $data[]= array('value'=>intval($count1),'label'=>'火警 '.$parent1.'%','tip'=>$parent1.'%');
	        $data[]= array('value'=>intval($count2),'label'=>'其它 '.$parent2.'%','tip'=>$parent2.'%');
	        $data[]= array('value'=>intval($count3),'label'=>'误报 '.$parent3.'%','tip'=>$parent3.'%');
	        $list = array(
        	    'title'=>array(
        	        'text'=>$year.'年'.$month.'月新增报警类型统计报表',
        	        "style"=>"{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
        	    ),
        	    'elements'=>array(
        	        array(
        	        'type'=>'pie',
        	        'alpha'=>0.5,
        	        'colour'=>'#9933CC',
        	        'colours'=>array('#d01f3c','#356aa0','#C79810'),
        	        'font-size'=>18,
        	        //"animate":		[ { "type": "fade" }],
        	        'on-show'=>array('type'=>'grow-up'),
        	        'values'=>$data
        	        )
        	    )
    	    );
	    }
	    $this->assign('data',json_encode($list));
	    $this->assign('year',$year);
	    $this->assign('month',$month);
	    $this->assign('type',$type);
	    if(isset($_GET['print']))
	    {
	        $this->display(MODULE_NAME . '_' . ACTION_NAME .'_print');
	    }
	    else 
	    {
		    $this->display();
	    }
	}
	function alpha()
	{
	    $this->pagename='烟感器统计';
	    $this->assign('pagename',$this->pagename);
	    $mod = D('Home.Alpha');
	    $year = isset($_GET['year'])?intval($_GET['year']):date('Y',time());
	    $i=0;
	    $data = array();
	    //获取所有月份的统计数据
	    $total = 0;
	    for($i=0;$i<12;$i++)
	    {
	        $where = 'year(from_unixtime(fogsensorinfo_createtime ))='.$year.' and month( from_unixtime(fogsensorinfo_createtime ))='.($i+1);
	        $count = $mod->where($where)->count();
	        $total += $count;
	        $data[]=array('value'=>$count,'label'=>($i+1).'月','tip'=>'');
	    }
	    for($i=0;$i<12;$i++)
	    {
	        $parent = ceil(($data[$i]['value']/$total)*100);
	        $data[$i]['tip']=$parent.'%';
	    }
	    $list = array(
        	    'title'=>array(
        	        'text'=>$year.'年新增烟感器统计报表',
        	        "style"=>"{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
        	    ),
        	    'elements'=>array(
        	        array(
        	        'type'=>'pie',
        	        'alpha'=>0.5,
        	        'colours'=>array('#FFCC00','#CCFF00','#00FFCC','#00FFCC','#9900FF'
        	        ,'#FF00FF','#FF0066','#808080','#090909','#4DD52B','#FF0000','#FF0033'),
        	        'font-size'=>10,
        	        //"animate":		[ { "type": "fade" }],
        	        'on-show'=>array('type'=>'grow-up'),
        	        'values'=>$data
        	        )
        	    )
    	    );
	    $this->assign('data',json_encode($list));
	    $this->assign('year',$year);
	    if(isset($_GET['print']))
	    {
	        $this->display(MODULE_NAME . '_' . ACTION_NAME .'_print');
	    }
	    else 
	    {
		    $this->display();
	    }
	}
	
}
?>
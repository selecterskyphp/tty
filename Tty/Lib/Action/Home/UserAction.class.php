<?php
if(!defined("TTY")) exit("Access Denied");
class UserAction extends UserbaseAction
{
    public $dao;
    function _initialize()
    {
        $this->dao = D('Home.'.MODULE_NAME);
        $this->pagename = '会员首页';
        parent::_initialize();
    }
    public function index()
    {
        $devices = D('Home.'.'Devices');//智能电话
        $devices_d = D('Home.'.'DevicesDetect');//智能电话检测
        $alpha = D('Home.'.'Alpha');//烟感器
        $alpha_d = D('Home.'.'AlphaDetect');//烟感器检测
        $firelist = D('Home.'.'FireList');
        $company_type = D('Home.CompanyType');
        $u = $this->login;
        $list = array();
        if($this->login['groupid'] == 2)
        {
            //消防用户
            //商户总数
            $where = 'domaininfo_id='.$u['domaininfo_id'].' and userinfo_rankpath=\''.$u['rankpath'].'\' and userrole_id=3';
            $list['user'] = $this->dao->where($where)->count();
            
            //智能电话总数
            $where = 'userinfo_id in(select userinfo_id from '.C('DB_PREFIX').'user_info where '.$where.')';
            $list['devices'] = $devices->where($where)->count();
            //var_dump($devices->getLastSql());
            //烟感器总数
            $list['alpha'] = $alpha->where($where)->count();
            //告警总数
            $list['firelist'] = $firelist->where($where)->count();
            //误报总数
            $list['firelist_error'] = $firelist->where($where.' and firerecordinfo_type=3')->count();
            $list['firelist_pre'] = intval(($list['firelist_error']/$list['firelist'])*100);
            //商户/企业总数
            $list['user'] = $this->dao->where($where)->count();
            //制造业总数
            $company_type_list = $company_type->select();
            
            for($i=0;$i<count($company_type_list);$i++)
            {
                $count = $this->dao->where($where.' and companytype_id='.$company_type_list[$i]['id'])->count();
                $company_type_list[$i]['count']=$count;
            }
            $list['user_type'] = $company_type_list;
            //var_dump($list['user_type']);
            //本月巡检电话
            $sql = "SELECT COUNT(*) AS tp_count FROM `tbl_telecomphone_detect` WHERE telecomphoneinfo_id in(select telecomphoneinfo_id from tbl_telecomphone_info where userinfo_id in(select userinfo_id from tbl_user_info where userinfo_provinceid=6 and userinfo_rankpath='1,3' and userrole_id=2)) and telecomphonedetect_type=1 LIMIT 1";
            $res=$devices_d->query($sql);
            $list['check_month']=$res[0]['tp_count'];
            
            //本月巡检电话故障
             $sql = "SELECT COUNT(*) AS tp_count FROM `tbl_telecomphone_detect` WHERE telecomphoneinfo_id in(select telecomphoneinfo_id from tbl_telecomphone_info where userinfo_id in(select userinfo_id from tbl_user_info where userinfo_provinceid=6 and userinfo_rankpath='1,3' and userrole_id=2)) and telecomphonedetect_type=1 and telecomphonedetect_result=3 LIMIT 1";
            $res=$devices_d->query($sql);
            $list['check_error_month']=$res[0]['tp_count'];
            //$list['check_error_month']=$devices_d->where($where.' and telecomphonedetect_type=1 and telecomphonedetect_result=3')->count();
            
            $device_where = $where . ' and (UNIX_TIMESTAMP()-telecomphoneinfo_createtime)<1000*60*60*24*30';
            //最近一个月的新增智能电话和烟感器
            $list['devices_month'] = $devices->where($device_where)->count();
            //烟感器总数
            $alpha_where = $where .' and (UNIX_TIMESTAMP()-fogsensorinfo_createtime)<1000*60*60*24*30';
            $list['alpha_month'] = $alpha->where($alpha_where)->count();
            //最近一个月的移机智能电话和烟感器
            //$list['devices_month_remove'] = $devicesremove->where($where)->count();
            $list['devices_month_remove'] = 0;
            //烟感器总数
            //$list['alpha_month_remove'] = $alpharemove->where($where)->count();
            $list['alpha_month_remove'] = 0;
            //
        }
        else if(3 == $this->login['groupid'])
        {
            //商户
            //智能电话总数
            $where = 'userinfo_id='.$this->login['id'];
            $list['devices'] = $devices->where($where)->count();
            //烟感器总数
            $list['alpha'] = $alpha->where($where)->count();
            
        }
        else if(4 == $this->login['groupid'])
        {
            $list['user'] = $this->dao->where('userrole_id=3')->count();
            //本月开户
            $list['user_month'] = $this->dao->where('userrole_id=3 and (UNIX_TIMESTAMP()-userinfo_createtime)<1000*60*60*24*30')->count();
            //var_dump($this->dao->getLastSql());
            $list['devices']=$devices->count();
            //本月新增电话机
            $list['devices_month']=$devices->where('(UNIX_TIMESTAMP()-telecomphoneinfo_createtime)<1000*60*60*24*30')->count();
            //本月派单 
            //$list['devices_status4_month']=$devices->where('(UNIX_TIMESTAMP()-checktime)<1000*60*60*24*30 and status=4')->count();
            $list['devices_status4_month'] = 0;
            //本月故障
            $list['devices_status2_month']=$devices_d->where('telecomphonedetect_type=1 and telecomphonedetect_result=3')->count();
            //本月修改
            //$list['devices_status5_month']=$devices->where('(UNIX_TIMESTAMP()-checktime)<1000*60*60*24*30 and status=5')->count();
            $list['devices_status5_month']=0;
            $list['alpha']=$alpha->count();
            //本月新增烟感器
            $list['alpha_month']=$alpha->where('(UNIX_TIMESTAMP()-fogsensorinfo_createtime)<1000*60*60*24*30')->count();
           // var_dump($alpha->getLastSql());
        }
        $this->assign('list',$list);
        
        if(isset($_GET['print']))
	    {
	        $this->display(MODULE_NAME . '_' . ACTION_NAME .'_print');
	    }
	    else 
	    {
		    $this->display();
	    }
    }
 
    public function basic()
	{
	    $this->pagename = '当前信息';
	    $this->assign('pagename',$this->pagename);
		$this->display();
	}
	public function pwd()
	{
	    $this->pagename = '登陆密码修改';
	    $this->assign('pagename',$this->pagename);
		$this->display();
	}
    public function show()
	{
	    $id = isset($_GET['id'])?intval($_GET['id']):0;
	    $data = $this->dao->getByuserinfo_id($id);
	    //var_dump($data,$this->dao->getLastSql());
	    $mod = D('Home.CompanyType');
	    $name = $mod->where('companytype_id='.$data['companytype_id'])->getField('name');
	    $data['companytype_name']=$name;
	    $this->assign('vo',$data);
		$this->display();
	}
	public function pwdupdate()
	{
		$pwd_old = isset($_POST['pwd_old'])?trim($_POST['pwd_old']):'';
		$pwd_new = isset($_POST['pwd_new'])?trim($_POST['pwd_new']):'';
		//$pwd_ag = isset($_POST['pwd_ag'])?trim($_POST['pwd_ag']):'';
		//var_dump($pwd_old,sysmd5($pwd_old),$this->login['password'],$this->login);
		//exit;
		if(sysmd5($pwd_old) != $this->login['passwd'])
		{
		    $this->error('旧密码输入不正确');
		}
		else 
		{
		    $data['id'] = $this->login['id'];
		    $data['passwd'] = sysmd5($pwd_new);
		    $m = D('Home.'.MODULE_NAME);
		    $m->data($data)->save();
		    $this->assign('jumpUrl',U('Index/logout'));
		    $this->success('修改密码成功');
		}
	}
	public function mail()
	{
	    $this->pagename = '邮件列表';
	    $this->assign('pagename',$this->pagename);
		$this->display();
	}
	public function mailsend()
	{
	    $this->pagename = '邮件发送';
	    $this->assign('pagename',$this->pagename);
		$this->display();
	}
	//$type 0时显示,其它值不显示,返回获取到的数据
	public function ulist($type=0)
	{
	    $this->pagename = '商户列表';
	    $this->assign('pagename',$this->pagename);
	    import ( '@.ORG.Page' );
	    $k = isset($_GET['keyword'])?trim($_GET['keyword']):'';
	    $area = isset($_GET['area'])?trim($_GET['area']):'';
	    $company_type = isset($_GET['company_type'])?intval($_GET['company_type']):0;
	    $this->assign($_GET);
	    $where = "domaininfo_id = ".$this->login['domaininfo_id'].' and userinfo_rankpath=\''.$this->login['rankpath'].'\' and userinfo_id<>'.$this->login['id'].' and userrole_id=3';
	    if(strlen($k)>0)
	    {
	        $where .= ' and userinfo_companyname like \'%'.$k.'%\'';
	    }
	    if($company_type > 0)
	    {
	        $where .= ' and companytype_id ='.$company_type;
	    }
		$count=$this->dao->where($where)->count();
		$page=new Page($count);
		$show=$page->show();
		//var_dump($count,$show);
		$this->assign("page",$show);
		$list=$this->dao->order('id')->where($where)
		->field('id,name,email,realname,telephone,mobile ,address ,companytype_id,companyname')
		->limit($page->firstRow.','.$page->listRows)->select();
		//var_dump($this->dao->getLastSql());
		$tmp = array();
		$mod = D('Home.'.'CompanyType');
		foreach ($list as $v)
		{
		    $name = $mod->where('companytype_id='.$v['companytype_id'])->getField('name');
		    $v['companytypename']=$name;
		    $tmp[]=$v;
		}
		$this->assign("list",$tmp);
		$mod = D('Home.CompanyType');
		$this->assign('company_type_list',$mod->select());
		if(0 == $type)
		{
		    $this->display();
		}
		else 
		{
		    return $tmp;
		}
	}
	public function edit()
	{
	    $this->pagename = '修改个人信息';
	    $this->assign('pagename',$this->pagename);
		$m = D('Home.'.'DomainInfo');
		$where = array();
		$where['parentsid'] = array('eq',0);
		$where['rank'] = array('eq',0);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id asc')->select();
		$this->assign('province',json_encode($data));
		unset($where);
		$where['rank'] = array('eq',1);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id asc')->select();
		$this->assign('city',json_encode($data));
		unset($where);
		$where['rank'] = array('eq',2);
		$where['enable'] = array('eq',1);
		$data = $m->where($where)->order('order desc,id asc')->select();
		$this->assign('area',json_encode($data));
		$mod = D('Home.'.'CompanyType');
		$type = $mod->select();
		$this->assign('company_type',$type);
		$this->display();
	}
	
	public function export()
	{
	    //'id,name,email,realname,telephone,mobile ,address ,companytype_id,companyname'
	    $dataCol = array('用户名称','邮箱','真实姓名','电话','手机','地址','公司名称','公司类型');
	    $data = $this->ulist(1);
	    $this->export_data($data,$dataCol);
	}
	
}
?>
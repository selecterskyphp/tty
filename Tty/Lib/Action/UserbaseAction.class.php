<?php
if(!defined("TTY")) exit("Access Denied");
class UserbaseAction extends BaseAction
{
    public $pagename = '';
	function _initialize()
	{
		parent::_initialize();
		if(!$this->isLogin || !isset($this->login))
		{
			$this->assign('jumpUrl','/');
			$this->error('您还没有登录，请先登录');
		}
		if(3 == $this->login['groupid'])
		{
		    $list = array();
		    //如果是商户,统计滚动信息
		    //需要更换电池的烟感器
		    $mod = D('Home.Alpha');
		    //电池有效天数为180天
		    $where = 'userinfo_id='.$this->login['id'].' and ((UNIX_TIMESTAMP()-fogsensorcell_createtime)/86400>1)';
		    $data = $mod->where($where)->select();
		    if($data)
		    {
		        foreach ($data as $v)
		        {
		            $list[]='<b>'.$v['location'].'</b>的烟感器需要更换电池';
		        }
		    }
		    //新的站内提示
		    
		    //$mod = M('guestbook');
		    //$where = 'receive_userid='.$this->login['id'].' and status=0';
		    //$count = $mod->where($where)->count();
		    //if($count>0)
		    //{
		        //$list[]='您有 <b>'.$count.'</b> 条未读站内信息';
		    //}
		    //
		    $mod = D('Home.DevicesDetect');
		    $sql = 'select a.* from '.C('DB_PREFIX').'telecomphone_info a,'.C('DB_PREFIX').'telecomphone_detect b where a.telecomphoneinfo_id = b.telecomphoneinfo_id';
		    $sql .= ' and a.userinfo_id='.$this->login['id'];
		    $sql .= ' and b.telecomphonedetect_result=3';
		    $data = $mod->query($sql);
		    if($data)
		    {
		        foreach ($data as $v)
		        {
		            $list[]='<b>'.$v['telecomphoneinfo_location'].'</b>位置的智能电话出现故障';
		        }
		    }
		    //
		    $mod = D('Home.AlphaDetect');
		    $sql = 'select a.* from '.C('DB_PREFIX').'fogsensor_info a,'.C('DB_PREFIX').'fogsensor_detect b where a.fogsensorinfo_id = b.fogsensorinfo_id';
		    $sql .= ' and a.userinfo_id='.$this->login['id'];
		    $sql .= ' and b.fogsensordetect_result=3';
		    $data = $mod->query($sql);
		    //var_dump($mod->getLastSql());
		    //exit;
		    if($data)
		    {
		        foreach ($data as $v)
		        {
		            $list[]='<b>'.$v['fogsensorinfo_location'].'</b>位置的烟感器出现故障';
		        }
		    }
		    $this->assign('marqueelist',$list);
		}
		$this->assign('css','user');
		$on = array();
		$on[MODULE_NAME . '_' . ACTION_NAME] = ' class="onRed"';
		$this->assign('on',$on);
		if(!isset($this->pagename))
		{
		    $this->pagename = '会员首页';
		}
		$this->assign('pagename',$this->pagename);
	}	
	
	function update() {
		
		$name = MODULE_NAME;
		$model = D ( 'Home.'.$name );
		
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
		
		if (false !== $model->save ()) {
			if($name == 'User')
			{
			    $where['id'] = array('eq',intval($_POST['id']));
				$r = $model->where($where)->find();
				$this->setLogin($r);
			}
			//var_dump($model->getLastSql());
			//exit;
			$this->success ('保存成功');
		} else 
		{
			$this->success ('保存失败: '.$model->getError());
		}
	}


	/**
     * 添加
     *
     */

	function add() {
		$name = MODULE_NAME;
		$this->assign($_GET);
		$this->display ($name.'_edit');
	}


	function insert() {

		if($_POST['setup']) $_POST['setup']=array2string($_POST['setup']);
		$name = MODULE_NAME;
		$model = D ('Home.'.$name);
		if (false === $model->create ()) {
			$this->error ( $model->getError () );
		}
		if ($model->add() !==false) {
		    $gid = isset($$_GET['gid'])?intval($gid):0;
		    if($gid >0)
		    {
			    $this->assign ( 'jumpUrl', U(MODULE_NAME.'/index',array('gid'=>$gid)) );
		    }
		    else
		    {
		        $this->assign ( 'jumpUrl', U(MODULE_NAME.'/index') );
		    }
			$this->success (L('add_ok'));
		} else {
			$this->error (L('add_error').': '.$model->getDbError());
		}
	}

	/**
     * 更新
     *
     */

	function edit() {
		$name = MODULE_NAME;
		$model = D ( 'Home.'.$name );
		$pk=ucfirst($model->getPk ());
		$id = $_REQUEST ['id'];
		if(empty($id))   $this->error(L('do_empty'));
		$do='getBy'.$pk;
		$vo = $model->$do ( $id );
		if($vo['setup']) $vo['setup']=string2array($vo['setup']);
		$this->assign ( 'vo', $vo );
		$this->assign($_GET);
		$this->display ();
	}
	/**
     * 删除
     *
     */
	function delete(){
		$name = MODULE_NAME;
		$model = D ( 'Home.'.$name );
		$pk = $model->getPk ();
		$id = $_REQUEST ['id'];
		if (isset ( $id )) {
			if(false!==$model->delete($id)){
				
				$this->success('删除成功');
			}else{
				$this->error('删除失败: '.$model->getDbError());
			}
		}else{
			$this->error ('没有选择任何记录');
		}
	}
   
	/**
     * 批量删除
     *
     */
	function deleteall(){

		$name = MODULE_NAME;
		$model = D ( 'Home.'.$name );
		$ids=$_POST['ids'];
		if(!empty($ids) && is_array($ids)){
			$id=implode(',',$ids);
			if(false!==$model->delete($id)){
				$this->success('删除成功');
			}else{
				$this->error('删除失败: '.$model->getDbError());
			}
		}else{
			$this->error('没有选择任何记录');
		}
	}
    function export_data($data,$col=null)
    {
        header("Content-type: text/html; charset=gb2312");
        $file = 'tmp.csv';
        $filename = CACHE_PATH . '/' .$file;
        $fp = fopen($filename,'w');
        if(is_array($col))
        {
            fputcsv($fp, $col);
        }
        //$str = implode(',', $col);
        foreach ($data as $v)
        {
            foreach($v as $k=>$vv)
            {
                if(strpos($k, 'time') !== false || strpos($k, 'date') !== false)
                {
                    //自动转换时间
                    $v[$k]=date('Y-m-d H:i:s',$v[$k]);
                }
                if(strpos($k, 'id')!==false)
                {
                    //自动过滤含有的ID
                    unset($v[$k]);
                }
            }
            fputcsv($fp, $v);
        }
        //exit;
        fclose($fp);
        $content = file_get_contents($filename);
        @unlink($filename);
        //var_dump($str);
        //exit;
        //$str = mb_convert_encoding($str,'gb2312','utf-8');
        $str = mb_convert_encoding($content,'gb2312','utf-8');
        //var_dump($str);
        import ( '@.ORG.Http' );
        Http::download('','tmp.csv',$str);
    }
    
    function getDevicesInfo($id,$type='number')
    {
        $mod = D('Home.Devices');
        $name = $mod->where('telecomphoneinfo_id='.$id)->getField($type);
        return $name;
    }
    
    function getAlphaInfo($id,$device_type='number')
    {
        $mod = D('Home.Alpha');
        $arr = $mod->where('fogsensorinfo_id='.$id)->field('telecomphoneinfo_id,name,location')->find();
        $arr['devices_name'] = $this->getDevicesInfo($arr['telecomphoneinfo_id'],$device_type);
        return $arr;
    }
}
?>
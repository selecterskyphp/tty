<?php
class CompanyTypeModel extends Model {
    protected $autoReplace = true;//是否自动替换字段里面的表名
    protected $autoAddTable = true;//是否自动在查询的字段前面加上表名
    protected $tableName = 'company_type';
    
	protected $fields = array(
	'_pk'=>'companytype_id',
	'_autoInc'=>true
	);
}
?>
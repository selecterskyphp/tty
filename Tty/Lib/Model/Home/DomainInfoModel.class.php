<?php
class DomainInfoModel extends Model {
    protected $autoReplace = true;//是否自动替换字段里面的表名
    protected $autoAddTable = true;//是否自动在查询的字段前面加上表名
    protected $tableName = 'domain_info';
	protected $fields = array(
	'_pk'=>'doamininfo_id',
	'_autoInc'=>true
	);
}
?>
<?php
class FireListModel extends Model {
    protected $autoReplace = true;//是否自动替换字段里面的表名
    protected $autoAddTable = true;//是否自动在查询的字段前面加上表名
    protected $tableName = 'firerecord_info';
	
	protected $fields = array(
	    '_pk'=>'firerecordinfo_id',
	    '_autoInc'=>true
	);
}
?>
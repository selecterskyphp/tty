<?php
class LogModel extends Model {
	protected $autoReplace = true;//是否自动替换字段里面的表名
    protected $autoAddTable = true;//是否自动在查询的字段前面加上表名
    protected $tableName = 'log';//是否自动在查询的字段前面加上表名
	protected $fields = array(
	    '_pk'=>'log_id',
	    '_autoInc'=>true
	);
	protected $_auto = array(
	    array('time','time',1,'function')
	);
}
?>
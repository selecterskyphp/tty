<?php
class UserModel extends Model {
    protected $autoReplace = true;//是否自动替换字段里面的表名
    protected $autoAddTable = true;//是否自动在查询的字段前面加上表名
    protected $tableName = 'user_info';
	protected $_map=array(
		//'id'=>'userinfo_id'
	);
	
	protected $fields = array(
	'_pk'=>'userinfo_id',
	'_autoInc'=>true
	);
	
	protected $_auto = array(
	    array('userinfo_passwd','sysmd5',3,'function')
	);
}
?>
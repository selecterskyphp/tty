<?php
if (!is_file('./config.php')) header("location: ./Install");
header("Content-type: text/html; charset=utf-8");
//define('RUNTIME_ALLINONE', true);
define('NO_CACHE_RUNTIME',True);//是否不缓存runtime
define('THINK_PATH', './Core');
define('APP_NAME', 'Tty');
define('APP_PATH', './Tty');
define('TTY', true);
require(THINK_PATH."/Core.php");
App::run();
?>
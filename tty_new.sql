-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 02 月 18 日 17:48
-- 服务器版本: 5.1.28
-- PHP 版本: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `tty_new`
--

-- --------------------------------------------------------

--
-- 表的结构 `tbl_access`
--

DROP TABLE IF EXISTS `tbl_access`;
CREATE TABLE IF NOT EXISTS `tbl_access` (
  `role_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `node_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` varchar(50) DEFAULT '',
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_article`
--

DROP TABLE IF EXISTS `tbl_article`;
CREATE TABLE IF NOT EXISTS `tbl_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(40) NOT NULL DEFAULT '',
  `title` varchar(120) NOT NULL DEFAULT '',
  `title_style` varchar(40) NOT NULL DEFAULT '',
  `keywords` varchar(120) NOT NULL DEFAULT '',
  `copyfrom` varchar(40) NOT NULL DEFAULT '',
  `fromlink` varchar(80) NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `content` text NOT NULL,
  `template` varchar(30) NOT NULL DEFAULT '',
  `thumb` varchar(100) NOT NULL DEFAULT '',
  `posid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `recommend` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `readgroup` varchar(255) NOT NULL DEFAULT '',
  `readpoint` int(10) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `url` varchar(50) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`id`,`status`,`listorder`),
  KEY `catid` (`id`,`catid`,`status`),
  KEY `listorder` (`id`,`catid`,`status`,`listorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_attachment`
--

DROP TABLE IF EXISTS `tbl_attachment`;
CREATE TABLE IF NOT EXISTS `tbl_attachment` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `catid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `id` int(8) unsigned NOT NULL DEFAULT '0',
  `filename` varchar(50) NOT NULL DEFAULT '',
  `filepath` varchar(80) NOT NULL DEFAULT '',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0',
  `fileext` char(10) NOT NULL DEFAULT '',
  `isimage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `isthumb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  `uploadip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_category`
--

DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(255) NOT NULL DEFAULT '',
  `catdir` varchar(30) NOT NULL DEFAULT '',
  `parentdir` varchar(50) NOT NULL DEFAULT '',
  `parentid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `moduleid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `module` char(24) NOT NULL DEFAULT '',
  `arrparentid` varchar(100) NOT NULL DEFAULT '',
  `arrchildid` varchar(100) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL DEFAULT '',
  `keywords` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ishtml` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `image` varchar(100) NOT NULL DEFAULT '',
  `child` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `url` varchar(100) NOT NULL DEFAULT '',
  `template_list` varchar(20) NOT NULL DEFAULT '',
  `template_show` varchar(20) NOT NULL DEFAULT '',
  `pagesize` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `readgroup` varchar(100) NOT NULL DEFAULT '',
  `listtype` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `listorder` (`listorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- 转存表中的数据 `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `catname`, `catdir`, `parentdir`, `parentid`, `moduleid`, `module`, `arrparentid`, `arrchildid`, `type`, `title`, `keywords`, `description`, `listorder`, `ishtml`, `ismenu`, `hits`, `image`, `child`, `url`, `template_list`, `template_show`, `pagesize`, `readgroup`, `listtype`, `lang`) VALUES
(1, '新闻中心', 'news', '', 0, 2, 'Article', '0', '1,2,3,10,16', 0, '公司新闻11', '公司新闻', '公司新闻', 0, 0, 1, 0, '', 1, '/index.php?m=Article&a=index&id=1', '', '', 0, '', 1, 0),
(2, '行业新闻', 'hangye', 'news/', 1, 2, 'Article', '0,1', '2', 0, '行业新闻', '行业新闻', '行业新闻', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=2', '', '', 0, '', 0, 0),
(3, '公司新闻', 'gongsi', 'news/', 1, 2, 'Article', '0,1', '3', 0, '公司新闻', '公司新闻', '公司新闻', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=3', '', '', 0, '', 0, 0),
(4, '产品展示', 'Product', '', 0, 3, 'Product', '0', '4,5,6,7,9,13', 0, '产品展示标题', '产品展示关键词', '产品展示栏目简介', 0, 0, 1, 0, '', 1, '/index.php?m=Product&a=index&id=4', '', '', 0, '', 0, 0),
(5, '产品分类1', 'cp1', 'Product/', 4, 3, 'Product', '0,4', '5', 0, '产品分类1', '产品分类1产品分类1', '产品分类1', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=5', '', '', 0, '2,3,4', 0, 0),
(6, '产品分类2', 'cp2', 'Product/', 4, 3, 'Product', '0,4', '6', 0, '产品分类2', '产品分类2', '产品分类2', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=6', '', '', 0, '', 0, 0),
(7, '产品分类3', 'cp3', 'Product/', 4, 3, 'Product', '0,4', '7', 0, '产品分类3', '产品分类3', '产品分类3', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=7', '', '', 0, '', 0, 0),
(8, '关于我们', 'about', '', 0, 1, 'Page', '0', '8,11,12', 0, '', '', '', 99, 0, 1, 0, '', 1, '/index.php?m=Page&a=index&id=8', '', '', 0, '', 0, 0),
(10, '行业资讯', 'zixun', 'news/', 1, 2, 'Article', '0,1', '10', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=10', '', '', 0, '', 0, 0),
(13, '产品分类5', 'cp5', 'Product/cp4/', 9, 3, 'Product', '0,4,9', '13', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Product&a=index&id=13', '', 'Product_show', 0, '', 0, 0),
(9, '产品分类4', 'cp4', 'Product/', 4, 3, 'Product', '0,4', '9,13', 0, '', '', '', 0, 0, 1, 0, '', 1, '/index.php?m=Product&a=index&id=9', '', '', 0, '2,3', 0, 0),
(11, '公司简介', 'info', 'about/', 8, 1, 'Page', '0,8', '11', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Page&a=index&id=11', '', '', 0, '', 0, 0),
(12, '联系我们', 'contactus', 'about/', 8, 1, 'Page', '0,8', '12', 0, '联系我们', '联系我们', '联系我们', 0, 0, 1, 0, '', 0, '/index.php?m=Page&a=index&id=12', '', '', 0, '', 0, 0),
(14, '图片展示', 'pics', '', 0, 4, 'Picture', '0', '14', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Picture&a=index&id=14', '', '', 0, '', 0, 0),
(17, '文档下载', 'down', '', 0, 5, 'Download', '0', '17', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Download&a=index&id=17', '', '', 0, '', 0, 0),
(16, '国内新闻', 'cnnews', 'news/', 1, 2, 'Article', '0,1', '16', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Article&a=index&id=16', '', '', 0, '', 0, 0),
(18, '信息反馈', 'Feedback', 'Guestbook/', 19, 34, 'Feedback', '0,19', '18', 0, '', '', '', 0, 0, 1, 0, '', 0, '/index.php?m=Feedback&a=index&id=18', '', '', 0, '', 0, 0),
(19, '在线留言', 'Guestbook', '', 0, 36, 'Guestbook', '0', '19,18', 0, '', '', '', 0, 0, 1, 0, '', 1, '/index.php?m=Guestbook&a=index&id=19', '', '', 5, '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_company_type`
--

DROP TABLE IF EXISTS `tbl_company_type`;
CREATE TABLE IF NOT EXISTS `tbl_company_type` (
  `companytype_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '企业性质表ID',
  `companytype_name` varchar(64) NOT NULL COMMENT '企业类型信息',
  PRIMARY KEY (`companytype_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='企业性质表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `tbl_company_type`
--

INSERT INTO `tbl_company_type` (`companytype_id`, `companytype_name`) VALUES
(1, '制造业'),
(2, '纺织业');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_config`
--

DROP TABLE IF EXISTS `tbl_config`;
CREATE TABLE IF NOT EXISTS `tbl_config` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `varname` varchar(20) NOT NULL DEFAULT '',
  `info` varchar(100) NOT NULL DEFAULT '',
  `groupid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `value` text NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- 转存表中的数据 `tbl_config`
--

INSERT INTO `tbl_config` (`id`, `varname`, `info`, `groupid`, `value`, `type`) VALUES
(1, 'site_name', '网站名称', 2, 'tty', 2),
(2, 'site_url', '网站网址', 2, 'http://tty', 2),
(3, 'logo', '网站LOGO', 2, './Public/Images/logo.gif', 2),
(4, 'site_company_name', '企业名称', 2, 'tty', 2),
(5, 'site_email', '站点邮箱', 2, 'admin@tty.cn', 2),
(6, 'site_contact_name', '联系人', 2, 'chengong', 2),
(7, 'site_tel', '联系电话', 2, '0571-5022625', 2),
(8, 'site_mobile', '手机号码', 2, '13292793176', 2),
(9, 'site_fax', '传真号码', 2, '0571-5022625', 2),
(10, 'site_address', '公司地址', 2, '浙江', 2),
(11, 'qq', '客服QQ', 2, '9474785', 2),
(12, 'seo_title', '网站标题', 3, 'seo_title', 2),
(13, 'seo_keywords', '关键词', 3, '', 2),
(14, 'seo_description', '网站简介', 3, 'tty', 2),
(15, 'mail_type', '邮件发送模式', 4, '1', 2),
(16, 'mail_server', '邮件服务器', 4, 'smtp.abc.cn', 2),
(17, 'mail_port', '邮件发送端口', 4, '25', 2),
(18, 'mail_from', '发件人地址', 4, 'admin@abc.cn', 2),
(19, 'mail_auth', 'AUTH LOGIN验证', 4, '1', 2),
(20, 'mail_user', '验证用户名', 4, 'admin@abc.cn', 2),
(21, 'mail_password', '验证密码', 4, '', 2),
(22, 'attach_maxsize', '允许上传附件大小', 5, '5200000', 1),
(23, 'attach_allowext', '允许上传附件类型', 5, 'jpg,jpeg,gif,png,doc,docx,rar,zip,swf', 2),
(24, 'watermark_enable', '是否开启图片水印', 5, '1', 1),
(25, 'watemard_text', '水印文字内容', 5, 'tty', 2),
(26, 'watemard_text_size', '文字大小', 5, '18', 1),
(27, 'watemard_text_color', 'watemard_text_color', 5, '#FFFFFF', 2),
(28, 'watemard_text_face', '字体', 5, 'elephant.ttf', 2),
(29, 'watermark_minwidth', '图片最小宽度', 5, '300', 1),
(30, 'watermark_minheight', '水印最小高度', 5, '300', 1),
(31, 'watermark_img', '水印图片名称', 5, 'mark.png', 2),
(32, 'watermark_pct', '水印透明度', 5, '80', 1),
(33, 'watermark_quality', 'JPEG 水印质量', 5, '100', 1),
(34, 'watermark_pospadding', '水印边距', 5, '10', 1),
(35, 'watermark_pos', '水印位置', 5, '9', 1),
(36, 'PAGE_LISTROWS', '列表分页数', 6, '15', 1),
(37, 'URL_MODEL', 'URL访问模式', 6, '0', 1),
(38, 'URL_PATHINFO_DEPR', '参数分割符', 6, '/', 2),
(39, 'URL_HTML_SUFFIX', 'URL伪静态后缀', 6, '.html', 2),
(40, 'TOKEN_ON', '令牌验证', 6, '1', 1),
(41, 'TOKEN_NAME', '令牌表单字段', 6, '__hash__', 2),
(42, 'TMPL_CACHE_ON', '模板编译缓存', 6, '0', 1),
(43, 'TMPL_CACHE_TIME', '模板缓存有效期', 6, '-1', 1),
(44, 'HTML_CACHE_ON', '静态缓存', 6, '0', 1),
(45, 'HTML_CACHE_TIME', '缓存有效期', 6, '60', 1),
(46, 'HTML_READ_TYPE', '缓存读取方式', 6, '0', 1),
(47, 'HTML_FILE_SUFFIX', '静态文件后缀', 6, '.html', 2),
(48, 'ADMIN_ACCESS', 'ADMIN_ACCESS', 6, '793f221d9844ed1ecfa72f23efd978ce', 2),
(49, 'DEFAULT_THEME', '默认模板', 6, 'Default', 2),
(50, 'HOME_ISHTML', '首页生成html', 6, '0', 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_domain_info`
--

DROP TABLE IF EXISTS `tbl_domain_info`;
CREATE TABLE IF NOT EXISTS `tbl_domain_info` (
  `domaininfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '区域信息表ID',
  `domaininfo_name` varchar(128) NOT NULL COMMENT '区域名称',
  `domaininfo_parentsid` int(11) unsigned NOT NULL COMMENT '区域父ID',
  `domaininfo_rank` tinyint(3) unsigned NOT NULL COMMENT '区域等级：0表示省，1表示市，2表示区县',
  `domaininfo_rankpath` varchar(128) DEFAULT NULL COMMENT '区域层次路径',
  `domaininfo_enable` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '区域使能：0表示使能；1表示去使能',
  `domaininfo_order` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '区域排序字段：0表示不排序',
  PRIMARY KEY (`domaininfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='区域信息表' AUTO_INCREMENT=103 ;

--
-- 转存表中的数据 `tbl_domain_info`
--

INSERT INTO `tbl_domain_info` (`domaininfo_id`, `domaininfo_name`, `domaininfo_parentsid`, `domaininfo_rank`, `domaininfo_rankpath`, `domaininfo_enable`, `domaininfo_order`) VALUES
(1, '浙江', 0, 0, '0', 1, 0),
(2, '杭州市', 1, 1, '1', 1, 0),
(3, '上城区', 2, 2, '1,2', 1, 0),
(4, '下城区', 2, 2, '1,2', 1, 0),
(5, '江干区', 2, 2, '1,2', 1, 0),
(6, '拱墅区', 2, 2, '1,2', 1, 0),
(7, '西湖区', 2, 2, '1,2', 1, 0),
(8, '滨江区', 2, 2, '1,2', 1, 0),
(9, '萧山区', 2, 2, '1,2', 1, 0),
(10, '余杭区', 2, 2, '1,2', 1, 0),
(11, '桐庐县', 2, 2, '1,2', 1, 0),
(12, '淳安县', 2, 2, '1,2', 1, 0),
(13, '建德市', 2, 2, '1,2', 1, 0),
(14, '富阳市', 2, 2, '1,2', 1, 0),
(15, '临安市', 2, 2, '1,2', 1, 0),
(16, '宁波市', 1, 1, '1', 1, 0),
(17, '海曙区', 16, 2, '1,16', 1, 0),
(18, '江东区', 16, 2, '1,16', 1, 0),
(19, '江北区', 16, 2, '1,16', 1, 0),
(20, '北仑区', 16, 2, '1,16', 1, 0),
(21, '镇海区', 16, 2, '1,16', 1, 0),
(22, '鄞州区', 16, 2, '1,16', 1, 0),
(23, '象山县', 16, 2, '1,16', 1, 0),
(24, '宁海县', 16, 2, '1,16', 1, 0),
(25, '余姚市', 16, 2, '1,16', 1, 0),
(26, '慈溪市', 16, 2, '1,16', 1, 0),
(27, '奉化市', 16, 2, '1,16', 1, 0),
(28, '温州市', 1, 1, '1', 1, 0),
(29, '鹿城区', 28, 2, '1,28', 1, 0),
(30, '龙湾区', 28, 2, '1,28', 1, 0),
(31, '瓯海区', 28, 2, '1,28', 1, 0),
(32, '洞头县', 28, 2, '1,28', 1, 0),
(33, '永嘉县', 28, 2, '1,28', 1, 0),
(34, '平阳县', 28, 2, '1,28', 1, 0),
(35, '苍南县', 28, 2, '1,28', 1, 0),
(36, '文成县', 28, 2, '1,28', 1, 0),
(37, '泰顺县', 28, 2, '1,28', 1, 0),
(38, '瑞安市', 28, 2, '1,28', 1, 0),
(39, '乐清市', 28, 2, '1,28', 1, 0),
(40, '嘉兴市', 1, 1, '1', 1, 0),
(41, '秀城区', 40, 2, '1,40', 1, 0),
(42, '秀洲区', 40, 2, '1,40', 1, 0),
(43, '嘉善县', 40, 2, '1,40', 1, 0),
(44, '海盐县', 40, 2, '1,40', 1, 0),
(45, '海宁市', 40, 2, '1,40', 1, 0),
(46, '平湖市', 40, 2, '1,40', 1, 0),
(47, '桐乡市', 40, 2, '1,40', 1, 0),
(48, '湖州市', 1, 1, '1', 1, 0),
(49, '吴兴区', 48, 2, '1,48', 1, 0),
(50, '南浔区', 48, 2, '1,48', 1, 0),
(51, '德清县', 48, 2, '1,48', 1, 0),
(52, '长兴县', 48, 2, '1,48', 1, 0),
(53, '安吉县', 48, 2, '1,48', 1, 0),
(54, '绍兴市', 1, 1, '1', 1, 0),
(55, '越城区', 54, 2, '1,54', 1, 0),
(56, '绍兴县', 54, 2, '1,54', 1, 0),
(57, '新昌县', 54, 2, '1,54', 1, 0),
(58, '诸暨市', 54, 2, '1,54', 1, 0),
(59, '上虞市', 54, 2, '1,54', 1, 0),
(60, '嵊州市', 54, 2, '1,54', 1, 0),
(61, '金华市', 1, 1, '1', 1, 0),
(62, '婺城区', 61, 2, '1,61', 1, 0),
(63, '金东区', 61, 2, '1,61', 1, 0),
(64, '武义县', 61, 2, '1,61', 1, 0),
(65, '浦江县', 61, 2, '1,61', 1, 0),
(66, '磐安县', 61, 2, '1,61', 1, 0),
(67, '兰溪市', 61, 2, '1,61', 1, 0),
(68, '义乌市', 61, 2, '1,61', 1, 0),
(69, '东阳市', 61, 2, '1,61', 1, 0),
(70, '永康市', 61, 2, '1,61', 1, 0),
(71, '衢州市', 1, 1, '1', 1, 0),
(72, '柯城区', 71, 2, '1,71', 1, 0),
(73, '衢江区', 71, 2, '1,71', 1, 0),
(74, '常山县', 71, 2, '1,71', 1, 0),
(75, '开化县', 71, 2, '1,71', 1, 0),
(76, '龙游县', 71, 2, '1,71', 1, 0),
(77, '江山市', 71, 2, '1,71', 1, 0),
(78, '舟山市', 1, 1, '1', 1, 0),
(79, '定海区', 78, 2, '1,78', 1, 0),
(80, '普陀区', 78, 2, '1,78', 1, 0),
(81, '岱山县', 78, 2, '1,78', 1, 0),
(82, '嵊泗县', 78, 2, '1,78', 1, 0),
(83, '台州市', 1, 1, '1', 1, 0),
(84, '椒江区', 83, 2, '1,83', 1, 0),
(85, '黄岩区', 83, 2, '1,83', 1, 0),
(86, '路桥区', 83, 2, '1,83', 1, 0),
(87, '玉环县', 83, 2, '1,83', 1, 0),
(88, '三门县', 83, 2, '1,83', 1, 0),
(89, '天台县', 83, 2, '1,83', 1, 0),
(90, '仙居县', 83, 2, '1,83', 1, 0),
(91, '温岭市', 83, 2, '1,83', 1, 0),
(92, '临海市', 83, 2, '1,83', 1, 0),
(93, '丽水市', 1, 1, '1', 1, 0),
(94, '莲都区', 93, 2, '1,93', 1, 0),
(95, '青田县', 93, 2, '1,93', 1, 0),
(96, '缙云县', 93, 2, '1,93', 1, 0),
(97, '遂昌县', 93, 2, '1,93', 1, 0),
(98, '松阳县', 93, 2, '1,93', 1, 0),
(99, '云和县', 93, 2, '1,93', 1, 0),
(100, '庆元县', 93, 2, '1,93', 1, 0),
(101, '景宁畲族自治县', 93, 2, '1,93', 1, 0),
(102, '龙泉市', 93, 2, '1,93', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_field`
--

DROP TABLE IF EXISTS `tbl_field`;
CREATE TABLE IF NOT EXISTS `tbl_field` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `field` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `tips` varchar(150) NOT NULL DEFAULT '',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `minlength` int(10) unsigned NOT NULL DEFAULT '0',
  `maxlength` int(10) unsigned NOT NULL DEFAULT '0',
  `pattern` varchar(255) NOT NULL DEFAULT '',
  `errormsg` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(20) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `setup` mediumtext NOT NULL,
  `ispost` tinyint(1) NOT NULL DEFAULT '0',
  `unpostgroup` varchar(60) NOT NULL DEFAULT '',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=103 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_firecaller_group`
--

DROP TABLE IF EXISTS `tbl_firecaller_group`;
CREATE TABLE IF NOT EXISTS `tbl_firecaller_group` (
  `firecallergroup_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '群呼电话组ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `firecallergroup_name` varchar(128) NOT NULL COMMENT '群呼电话组名称',
  PRIMARY KEY (`firecallergroup_id`),
  KEY `FK_Reference_23` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='群呼电话组表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `tbl_firecaller_group`
--

INSERT INTO `tbl_firecaller_group` (`firecallergroup_id`, `userinfo_id`, `firecallergroup_name`) VALUES
(1, 2, '11112'),
(2, 2, 'aaa');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_firecaller_info`
--

DROP TABLE IF EXISTS `tbl_firecaller_info`;
CREATE TABLE IF NOT EXISTS `tbl_firecaller_info` (
  `firecallerinfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '群呼电话信息表ID',
  `firecallergroup_id` int(11) unsigned NOT NULL COMMENT '群呼电话组ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `firecallerinfo_name` varchar(128) DEFAULT NULL COMMENT '群呼电话名称',
  `firecallerinfo_number` varchar(32) NOT NULL COMMENT '群呼电话号码',
  `firecallerinfo_ismain` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为主叫群呼电话：0表示不是主叫群呼电话；1表示是主叫群呼电话',
  `firecallerinfo_remark` varchar(128) DEFAULT NULL COMMENT '群呼电话备注信息',
  PRIMARY KEY (`firecallerinfo_id`),
  KEY `FK_Reference_15` (`firecallergroup_id`),
  KEY `FK_Reference_24` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='群呼电话信息表' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_firecaller_info`
--

INSERT INTO `tbl_firecaller_info` (`firecallerinfo_id`, `firecallergroup_id`, `userinfo_id`, `firecallerinfo_name`, `firecallerinfo_number`, `firecallerinfo_ismain`, `firecallerinfo_remark`) VALUES
(1, 1, 2, '陈甲新', '23242424', 0, 'adad'),
(2, 1, 2, '2432', '2424', 1, '24'),
(3, 2, 2, 'aa', 'b', 1, 'b');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_firecontroller_group`
--

DROP TABLE IF EXISTS `tbl_firecontroller_group`;
CREATE TABLE IF NOT EXISTS `tbl_firecontroller_group` (
  `firecontrollergroup_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '消防责任人组ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `firecontrollergroup_name` varchar(128) NOT NULL COMMENT '消防责任人组名称',
  PRIMARY KEY (`firecontrollergroup_id`),
  KEY `FK_Reference_22` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='消防责任人组表' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_firecontroller_group`
--

INSERT INTO `tbl_firecontroller_group` (`firecontrollergroup_id`, `userinfo_id`, `firecontrollergroup_name`) VALUES
(3, 2, 'sdfs'),
(5, 2, '33333');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_firecontroller_info`
--

DROP TABLE IF EXISTS `tbl_firecontroller_info`;
CREATE TABLE IF NOT EXISTS `tbl_firecontroller_info` (
  `firecontrollerinfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '消防责任人信息表ID',
  `firecontrollergroup_id` int(11) unsigned NOT NULL COMMENT '消防责任人组ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `firecontrollerinfo_name` varchar(128) NOT NULL COMMENT '消防责任人姓名',
  `firecontrollerinfo_ismain` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否为首要消防责任人：0表示不是首要消防责任人；1表示是首要消防责任人',
  `firecontrollerinfo_number` varchar(32) NOT NULL COMMENT '消防责任人电话号码',
  `firecontrollerinfo_location` varchar(128) DEFAULT NULL COMMENT '消防责任人地址',
  PRIMARY KEY (`firecontrollerinfo_id`),
  KEY `FK_Reference_25` (`firecontrollergroup_id`),
  KEY `FK_Reference_26` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='消防责任人信息表' AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `tbl_firecontroller_info`
--

INSERT INTO `tbl_firecontroller_info` (`firecontrollerinfo_id`, `firecontrollergroup_id`, `userinfo_id`, `firecontrollerinfo_name`, `firecontrollerinfo_ismain`, `firecontrollerinfo_number`, `firecontrollerinfo_location`) VALUES
(1, 1, 2, '联系人1', 1, '电话号码', '地址'),
(2, 1, 2, '陈甲新', 0, '23424243', '滨江'),
(3, 2, 2, '陈甲新', 0, '234243', '党派'),
(5, 3, 2, 'sfs112', 1, 'sfsf', 'sf'),
(6, 3, 2, 'sss1', 0, 'sss', 'sss'),
(7, 5, 2, 'we', 1, '312312', '32131');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_firerecord_info`
--

DROP TABLE IF EXISTS `tbl_firerecord_info`;
CREATE TABLE IF NOT EXISTS `tbl_firerecord_info` (
  `firerecordinfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '火警信息表ID',
  `telecomphoneinfo_id` int(11) unsigned NOT NULL COMMENT '智能电话信息表ID',
  `telecomphoneinfo_number` varchar(30) NOT NULL COMMENT '报警电话号码',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `firerecordinfo_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '火警上报时间',
  `firerecordinfo_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '火警上报类型：1为火警；2为测试；3为误报',
  `firerecordinfo_remark` varchar(128) DEFAULT NULL COMMENT '火警上报说明',
  PRIMARY KEY (`firerecordinfo_id`),
  KEY `FK_Reference_32` (`telecomphoneinfo_id`),
  KEY `FK_Reference_33` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='火警信息表' AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `tbl_firerecord_info`
--

INSERT INTO `tbl_firerecord_info` (`firerecordinfo_id`, `telecomphoneinfo_id`, `telecomphoneinfo_number`, `userinfo_id`, `firerecordinfo_time`, `firerecordinfo_type`, `firerecordinfo_remark`) VALUES
(1, 1, '主叫号码', 2, 1325599627, 1, '火警'),
(2, 1, '主叫号码', 2, 1325599627, 2, '测试'),
(3, 1, '主叫号码', 2, 1325599627, 3, '误报');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_fogsensor_detect`
--

DROP TABLE IF EXISTS `tbl_fogsensor_detect`;
CREATE TABLE IF NOT EXISTS `tbl_fogsensor_detect` (
  `fogsensordetect_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '烟感器检测结果表ID',
  `fogsensorinfo_id` int(11) unsigned NOT NULL COMMENT '烟感器信息表ID',
  `fogsensordetect_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '烟感器检测类型：0为自检、1为巡检',
  `fogsensordetect_sign` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '烟感器自检信号量：0表示无需立即自检；1表示需要立即自检。该字段对巡检无意义',
  `fogsensordetect_result` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '烟感器检测结果：0为正常；1为检测中；2为未知；3为故障',
  `fogsensordetect_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '烟感器检测时间',
  PRIMARY KEY (`fogsensordetect_id`),
  KEY `FK_Reference_17` (`fogsensorinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='烟感器检测结果表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tbl_fogsensor_detect`
--

INSERT INTO `tbl_fogsensor_detect` (`fogsensordetect_id`, `fogsensorinfo_id`, `fogsensordetect_type`, `fogsensordetect_sign`, `fogsensordetect_result`, `fogsensordetect_time`) VALUES
(1, 1, 0, 1, 3, 1329585647);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_fogsensor_info`
--

DROP TABLE IF EXISTS `tbl_fogsensor_info`;
CREATE TABLE IF NOT EXISTS `tbl_fogsensor_info` (
  `fogsensorinfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '烟感器信息表ID',
  `telecomphoneinfo_id` int(11) unsigned NOT NULL COMMENT '智能电话信息表ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `fogsensorinfo_name` varchar(128) NOT NULL COMMENT '烟感器名称',
  `fogsensorinfo_location` varchar(128) DEFAULT NULL COMMENT '烟感器安装位置',
  `fogsensorinfo_createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '烟感器安装时间',
  `fogsensorcell_createtime` int(11) unsigned NOT NULL COMMENT '烟感电池安装时间',
  PRIMARY KEY (`fogsensorinfo_id`),
  KEY `FK_Reference_18` (`telecomphoneinfo_id`),
  KEY `FK_Reference_34` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='烟感器信息表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tbl_fogsensor_info`
--

INSERT INTO `tbl_fogsensor_info` (`fogsensorinfo_id`, `telecomphoneinfo_id`, `userinfo_id`, `fogsensorinfo_name`, `fogsensorinfo_location`, `fogsensorinfo_createtime`, `fogsensorcell_createtime`) VALUES
(1, 1, 2, '烟感器2', '2221', 1325530883, 1326551312);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_guestbook`
--

DROP TABLE IF EXISTS `tbl_guestbook`;
CREATE TABLE IF NOT EXISTS `tbl_guestbook` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `send_name` varchar(20) NOT NULL,
  `send_userid` int(11) NOT NULL,
  `receive_userid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `replay_title` varchar(50) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `replay_content` mediumtext NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `tbl_guestbook`
--

INSERT INTO `tbl_guestbook` (`id`, `send_name`, `send_userid`, `receive_userid`, `title`, `replay_title`, `status`, `createtime`, `content`, `replay_content`, `ip`) VALUES
(1, 'xiaofang', 3, 2, 'tset', '', 0, 1325959385, 'stet', '', ''),
(2, 'xiaofang', 3, 2, 'tset', '', 1, 1325959411, 'stet', '', ''),
(5, 'test', 2, 3, '回复:tset', '', 0, 1325960990, 'asfasfdasdfasfdsfd', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_kefu`
--

DROP TABLE IF EXISTS `tbl_kefu`;
CREATE TABLE IF NOT EXISTS `tbl_kefu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL DEFAULT '',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `code` varchar(150) NOT NULL DEFAULT '',
  `skin` varchar(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_kefu`
--

INSERT INTO `tbl_kefu` (`id`, `status`, `listorder`, `createtime`, `name`, `type`, `code`, `skin`) VALUES
(1, 1, 4, 1306808546, '咨询电话', 4, '0571-5022625', '0'),
(2, 1, 3, 1306808546, '技术咨询', 1, '123456', 'q3'),
(3, 1, 3, 1306808886, 'QQ客服', 1, '123456', 'q3'),
(4, 1, 2, 1306811439, 'MSN客服', 2, 'aaa@msn.cn', 'm2'),
(5, 1, 0, 1306830001, '旺旺客服', 3, 'abc', 'w1');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_link`
--

DROP TABLE IF EXISTS `tbl_link`;
CREATE TABLE IF NOT EXISTS `tbl_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `listorder` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `logo` varchar(80) NOT NULL DEFAULT '',
  `siteurl` varchar(150) NOT NULL DEFAULT '',
  `typeid` smallint(5) unsigned NOT NULL,
  `linktype` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `siteinfo` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `tbl_link`
--

INSERT INTO `tbl_link` (`id`, `status`, `listorder`, `createtime`, `name`, `logo`, `siteurl`, `typeid`, `linktype`, `siteinfo`) VALUES
(1, 1, 0, 1306547518, 'tty', 'http://tty/Public/Images/logo.gif', 'http://tty', 2, 2, 'siteinfo'),
(2, 1, 0, 1306554684, 'test', '', 'http://www.test.cn', 2, 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_log`
--

DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE IF NOT EXISTS `tbl_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '操作日志表ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `log_info` varchar(256) NOT NULL COMMENT '日志信息',
  `log_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日志记录时间',
  `log_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '日志类型字段：1表示操作日志；2表示消防日志(包括商户更换电池以及自检操作)',
  PRIMARY KEY (`log_id`),
  KEY `FK_Reference_27` (`userinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='操作日志表' AUTO_INCREMENT=14 ;

--
-- 转存表中的数据 `tbl_log`
--

INSERT INTO `tbl_log` (`log_id`, `userinfo_id`, `log_info`, `log_time`, `log_type`) VALUES
(1, 3, 'xiaofang 退出会员管理中心', 1329583842, 1),
(2, 2, 'test 成功登录会员管理中心', 1329583867, 1),
(3, 2, '商户 test 对智能电话 <b>主叫号码</b> 进行了自检', 1329583881, 2),
(4, 2, '商户 test 对智能电话 <b>主叫号码</b> 下的烟感器 <b>烟感器2</b> 进行了自检', 1329583893, 2),
(5, 2, 'test 退出会员管理中心', 1329583921, 1),
(6, 3, 'xiaofang 成功登录会员管理中心', 1329583934, 1),
(7, 3, 'xiaofang 退出会员管理中心', 1329585147, 1),
(8, 2, 'test 成功登录会员管理中心', 1329585160, 1),
(9, 2, 'test 成功登录会员管理中心', 1329585560, 1),
(10, 2, '商户 test 对智能电话 <b>主叫号码</b> 下的烟感器 <b>烟感器2</b> 进行了自检', 1329585647, 2),
(11, 2, 'test 退出会员管理中心', 1329586320, 1),
(12, 3, 'xiaofang 成功登录会员管理中心', 1329586358, 1),
(13, 3, 'xiaofang 退出会员管理中心', 1329586727, 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_managedomain_info`
--

DROP TABLE IF EXISTS `tbl_managedomain_info`;
CREATE TABLE IF NOT EXISTS `tbl_managedomain_info` (
  `managedomaininfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理区域表ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户ID',
  `domaininfo_id` int(11) unsigned NOT NULL COMMENT '区域ID',
  `domaininfo_rankpath` varchar(128) NOT NULL COMMENT '区域层次路径',
  PRIMARY KEY (`managedomaininfo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理区域表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_managerole_info`
--

DROP TABLE IF EXISTS `tbl_managerole_info`;
CREATE TABLE IF NOT EXISTS `tbl_managerole_info` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_managerole_info`
--

INSERT INTO `tbl_managerole_info` (`id`, `name`, `status`, `remark`, `pid`, `listorder`) VALUES
(1, '超级管理员', 1, '超级管理', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_managerole_manageuser`
--

DROP TABLE IF EXISTS `tbl_managerole_manageuser`;
CREATE TABLE IF NOT EXISTS `tbl_managerole_manageuser` (
  `role_id` mediumint(9) unsigned DEFAULT '0',
  `user_id` char(32) DEFAULT '0',
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tbl_managerole_manageuser`
--

INSERT INTO `tbl_managerole_manageuser` (`role_id`, `user_id`) VALUES
(3, '2'),
(2, '3'),
(4, '4');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_manageuser_info`
--

DROP TABLE IF EXISTS `tbl_manageuser_info`;
CREATE TABLE IF NOT EXISTS `tbl_manageuser_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `realname` varchar(50) NOT NULL DEFAULT '',
  `question` varchar(50) NOT NULL DEFAULT '',
  `answer` varchar(50) NOT NULL DEFAULT '',
  `sex` tinyint(1) NOT NULL DEFAULT '0',
  `tel` varchar(50) NOT NULL DEFAULT '',
  `mobile` varchar(50) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(100) NOT NULL DEFAULT '',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0',
  `logintime` int(11) unsigned NOT NULL DEFAULT '0',
  `last_logintime` int(11) unsigned NOT NULL DEFAULT '0',
  `reg_ip` char(15) NOT NULL DEFAULT '',
  `last_ip` char(15) NOT NULL DEFAULT '',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `companyname` varchar(50) DEFAULT NULL,
  `areaid` int(11) NOT NULL,
  `zhengjian` char(30) NOT NULL,
  `province_path` char(30) NOT NULL,
  `province_id` int(11) NOT NULL,
  `xiaofang_name` char(10) NOT NULL,
  `xiaofang_tel` char(20) NOT NULL,
  `company_type_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `tbl_manageuser_info`
--

INSERT INTO `tbl_manageuser_info` (`id`, `groupid`, `username`, `password`, `email`, `realname`, `question`, `answer`, `sex`, `tel`, `mobile`, `fax`, `address`, `login_count`, `createtime`, `logintime`, `last_logintime`, `reg_ip`, `last_ip`, `status`, `companyname`, `areaid`, `zhengjian`, `province_path`, `province_id`, `xiaofang_name`, `xiaofang_tel`, `company_type_id`) VALUES
(1, 1, 'admin', 'a77e82e112ae8a1c543a4ad6ac16caa4084d016d', 'admin@tty.cn', 'admin', '1', '1', 1, '联系电话', '联系手机', '1', '1', 39, 1321669679, 1321669679, 1329581110, '127.0.0.1', '127.0.0.1', 1, '公司名称', 2, '', '', 0, '', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `model` char(20) NOT NULL DEFAULT '',
  `action` char(20) NOT NULL DEFAULT '',
  `data` char(50) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `parentid` (`parentid`),
  KEY `model` (`model`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- 转存表中的数据 `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `parentid`, `model`, `action`, `data`, `type`, `status`, `group_id`, `name`, `remark`, `listorder`) VALUES
(1, 0, 'Main', 'main', 'menuid=42', 1, 1, 0, '后台首页', '', 0),
(2, 0, 'Province', 'index', 'menuid=67', 1, 1, 0, '系统设置', '系统设置', 1),
(3, 0, 'Category', '', 'menuid=17', 1, 0, 0, '内容管理', '模型管理', 2),
(4, 0, 'Module', 'index', 'type=2&menuid=51', 1, 0, 0, '模块管理', '', 4),
(5, 0, 'User', '', 'menuid=9', 1, 1, 0, '会员管理', '', 90),
(7, 0, 'Template', 'index', 'menuid=60', 1, 0, 0, '模板管理', '', 99),
(39, 2, 'Menu', '', '', 1, 1, 0, '后台管理菜单', '后台管理菜单', 11),
(15, 39, 'Menu', 'add', '', 1, 1, 0, '添加菜单', '', 0),
(8, 50, 'Config', 'sys', '', 1, 1, 0, '系统参数', '', 0),
(32, 50, 'Config', 'seo', '', 1, 1, 0, 'SEO设置', '', 0),
(59, 50, 'Config', 'add', '', 1, 1, 0, '添加系统变量', '', 99),
(9, 5, 'User', '', '', 1, 1, 0, '管理员管理', '', 0),
(10, 9, 'User', 'add', '', 1, 1, 0, '添加管理员', '', 0),
(11, 5, 'Role', '', '', 1, 1, 0, '管理员组管理', '', 0),
(12, 11, 'Role', 'add', '', 1, 1, 0, '添加管理组', '', 0),
(13, 5, 'Node', '', '', 1, 0, 0, '权限节点管理', '', 0),
(14, 13, 'Node', 'add', '', 1, 1, 0, '添加权限节点', '', 0),
(16, 3, 'Module', '', '', 1, 1, 0, '模型管理', '', 99),
(17, 3, 'Category', '', '', 1, 1, 0, '栏目管理', '栏目管理', 1),
(18, 16, 'Module', 'add', '', 1, 1, 0, '添加模型', '', 0),
(19, 17, 'Category', 'add', '', 1, 1, 0, '添加栏目', '', 0),
(20, 3, 'Article', '', '', 1, 1, 0, '文章模型', '', 2),
(21, 20, 'Article', 'add', '', 1, 1, 0, '添加文章', '', 0),
(24, 23, 'Posid', 'add', '', 1, 1, 0, '添加推荐位', '', 0),
(25, 22, 'Product', 'add', '', 1, 1, 0, '添加产品', '', 0),
(28, 2, 'Type', '', '', 1, 0, 0, '类别管理', '', 6),
(29, 50, 'Config', 'mail', '', 1, 1, 0, '系统邮箱', '', 0),
(30, 50, 'Config', 'attach', '', 1, 1, 0, '附件配置', '', 0),
(31, 17, 'Category', 'repair_cache', '', 1, 1, 0, '恢复栏目数据', '', 0),
(33, 6, 'Createhtml', 'index', '', 1, 1, 0, '更新首页', '', 0),
(34, 6, 'Createhtml', 'Createlist', '', 1, 1, 0, '更新列表页', '', 0),
(35, 6, 'Createhtml', 'Createshow', '', 1, 1, 0, '更新内容页', '', 0),
(36, 6, 'Createhtml', 'Updateurl', '', 1, 1, 0, '更新内容页url', '', 0),
(37, 26, 'Picture', 'add', '', 1, 1, 0, '添加图片', '', 0),
(38, 27, 'Download', 'add', '', 1, 1, 0, '添加文件', '', 0),
(40, 1, 'Main', 'password', '', 1, 1, 0, '修改密码', '', 2),
(41, 1, 'Main', 'profile', 'menuid=67', 1, 1, 0, '个人资料', '', 3),
(42, 1, 'Main', 'main', '', 1, 1, 0, '后台首页', '', 1),
(43, 17, 'Category', 'add', '&type=1', 1, 1, 0, '添加外部链接', '', 0),
(44, 2, 'Database', '', '', 1, 0, 0, '数据库管理', '', 0),
(45, 44, 'Database', 'query', '', 1, 1, 0, '执行SQL语句', '', 0),
(46, 44, 'Database', 'recover', '', 1, 1, 0, '恢复数据库', '', 0),
(47, 1, 'Main', 'cache', 'menuid=47', 1, 1, 0, '更新缓存', '', 4),
(48, 51, 'Module', 'add', 'type=2', 1, 1, 0, '创建模块', '', 0),
(49, 3, 'Feedback', 'index', '', 1, 1, 0, '信息反馈', '信息反馈', 7),
(51, 4, 'Module', 'index', 'type=2', 1, 1, 0, '模块管理', '', 0),
(52, 28, 'Type', 'add', '', 1, 1, 0, '添加类别', '', 0),
(53, 4, 'Link', 'index', '', 1, 1, 0, '友情链接', '', 0),
(54, 53, 'Link', 'add', '', 1, 1, 0, '添加链接', '', 0),
(55, 3, 'Guestbook', 'index', '', 1, 1, 0, '在线留言', '', 8),
(57, 4, 'Kefu', 'index', '', 1, 1, 0, '在线客服', '', 0),
(58, 57, 'Kefu', 'add', '', 1, 1, 0, '添加客服', '', 0),
(60, 7, 'Template', 'index', '', 1, 1, 0, '模板管理', '', 0),
(61, 60, 'Template', 'add', '', 1, 1, 0, '添加模板', '', 0),
(62, 60, 'Template', 'index', 'type=css', 1, 1, 0, 'CSS管理', '', 0),
(63, 60, 'Template', 'index', 'type=js', 1, 1, 0, 'JS管理', '', 0),
(64, 60, 'Template', 'images', '', 1, 1, 0, '媒体文件管理', '', 0),
(65, 7, 'Theme', 'index', 'menuid=65', 1, 1, 0, '风格管理', '', 0),
(66, 65, 'Theme', 'upload', '', 1, 1, 0, '上传风格', '', 0),
(67, 2, 'Province', 'index', '', 1, 1, 0, '省市区管理', '', 0),
(68, 67, 'Province', 'add', '', 1, 1, 0, '添加', '', 0),
(82, 71, 'DevicesPeople', 'index', '', 1, 1, 0, '群呼电话管理', '', 3),
(70, 69, 'Address', 'add', '', 1, 1, 0, '添加街道', '', 0),
(71, 0, 'DevicesGroup', 'index', 'menuid=73', 1, 1, 0, '设备管理', '', 2),
(72, 71, 'Devices', 'index', '', 1, 1, 0, '智能电话管理', '', 2),
(73, 71, 'DevicesGroup', 'index', '', 1, 1, 0, '群呼组管理', '', 0),
(74, 71, 'FireGroup', 'index', '', 1, 1, 0, '消防组管理', '', 1),
(75, 71, 'FirePeople', 'index', '', 1, 1, 0, '消防员管理', '', 3),
(76, 73, 'DevicesGroup', 'add', '', 1, 1, 0, '增加', '', 0),
(77, 74, 'FireGroup', 'add', '', 1, 1, 0, '增加', '', 0),
(78, 72, 'Devices', 'add', '', 1, 1, 0, '增加', '', 0),
(79, 75, 'FirePeople', 'add', '', 1, 1, 0, '增加', '', 0),
(80, 71, 'Alpha', 'index', '', 1, 1, 0, '烟感器管理', '', 4),
(81, 80, 'Alpha', 'add', '', 1, 1, 0, '增加', '', 0),
(83, 82, 'DevicesPeople', 'add', '', 1, 1, 0, '增加', '', 0),
(84, 2, 'CompanyType', '', '', 1, 1, 0, '公司类型管理', '', 0),
(85, 84, 'CompanyType', 'add', '', 1, 1, 0, '添加公司类型', '', 0),
(86, 5, 'UserRole', '', '', 1, 1, 0, '用户组管理', '', 0),
(87, 86, 'UserRole', 'add', '', 1, 1, 0, '添加用户组', '', 0),
(88, 5, 'UserInfo', '', '', 1, 1, 0, '用户管理', '', 0),
(89, 88, 'UserInfo', 'add', '', 1, 1, 0, '用户添加', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_module`
--

DROP TABLE IF EXISTS `tbl_module`;
CREATE TABLE IF NOT EXISTS `tbl_module` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `issystem` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `listfields` varchar(255) NOT NULL DEFAULT '',
  `setup` text NOT NULL,
  `listorder` smallint(3) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `tbl_module`
--

INSERT INTO `tbl_module` (`id`, `title`, `name`, `description`, `type`, `issystem`, `listfields`, `setup`, `listorder`, `status`) VALUES
(1, '单页模型', 'Page', '单页模型', 1, 1, 'id,catid,url,title,title_style,keywords,description,thumb,createtime', '', 0, 1),
(2, '文章模型', 'Article', '新闻文章', 1, 1, 'id,catid,url,title,userid,username,hits,title_style,keywords,description,thumb,createtime', '', 0, 1),
(3, '产品模型', 'Product', '产品展示', 1, 1, 'id,catid,url,title,userid,username,hits,title_style,keywords,description,thumb,createtime,xinghao,price', '', 0, 1),
(4, '图片模型', 'Picture', '图片展示', 1, 1, 'id,catid,url,title,userid,username,hits,title_style,keywords,description,thumb,createtime', '', 0, 1),
(5, '下载模型', 'Download', '文件下载', 1, 1, 'id,catid,url,title,title_style,userid,username,hits,keywords,description,thumb,createtime,ext,size', '', 0, 1),
(6, '信息反馈', 'Feedback', '信息反馈', 1, 0, '*', '', 0, 1),
(7, '友情链接', 'Link', '友情链接', 2, 0, '*', '', 0, 1),
(8, '在线留言', 'Guestbook', '在线留言', 1, 0, '*', '', 0, 1),
(9, '在线客服', 'Kefu', '在线客服', 2, 0, '*', '', 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_node`
--

DROP TABLE IF EXISTS `tbl_node`;
CREATE TABLE IF NOT EXISTS `tbl_node` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `listorder` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`,`status`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- 转存表中的数据 `tbl_node`
--

INSERT INTO `tbl_node` (`id`, `name`, `title`, `status`, `remark`, `listorder`, `pid`, `level`) VALUES
(1, 'Admin', '后台管理', 1, '后台项目', 0, 0, 1),
(2, 'Index', '后台默认', 1, '控制器', 0, 1, 2),
(3, 'Config', '系统设置', 1, '控制器', 0, 1, 2),
(4, 'index', '站点配置', 1, '动作', 0, 3, 3),
(5, 'sys', '系统参数', 1, '动作', 0, 3, 3),
(6, 'seo', 'SEO设置', 1, '动作', 0, 3, 3),
(7, 'add', '添加变量', 1, '动作', 0, 3, 3),
(8, 'Menu', '菜单管理', 1, '菜单控制器', 0, 1, 2),
(9, 'index', '菜单列表', 1, '菜单列表-动作', 0, 8, 3),
(10, 'add', '添加菜单', 1, '添加菜单-动作', 0, 8, 3),
(11, 'index', '后台默认动作', 1, '后台默认动作', 0, 2, 3),
(12, 'Main', '后台首页', 1, '控制器', 0, 1, 2),
(13, 'profile', '个人资料', 1, '动作', 0, 12, 3),
(14, 'password', '修改密码', 1, '动作', 0, 12, 3),
(15, 'index', '后台首页', 1, '动作', 0, 12, 3),
(16, 'main', '欢迎首页', 1, '动作', 0, 12, 3),
(17, 'attach', '附件设置', 1, '动作', 0, 3, 3),
(18, 'mail', '系统邮箱', 1, '动作', 0, 3, 3),
(19, 'Posid', '推荐位', 1, '控制器', 0, 1, 2),
(20, 'index', '列表', 1, '动作', 0, 19, 3),
(21, 'Category', '模型管理', 1, '', 0, 1, 2),
(22, 'User', '会员管理', 1, '', 0, 1, 2),
(23, 'Createhtml', '网站更新', 1, '', 0, 1, 2),
(24, 'index', '栏目管理', 1, '', 0, 21, 3),
(25, 'index', '会员资料管理', 1, '', 0, 22, 3),
(26, 'index', '更新首页', 1, '', 0, 23, 3),
(27, 'Createlist', '更新列表页', 1, '', 0, 23, 3),
(28, 'Createshow', '更新内容页', 1, '', 0, 23, 3),
(29, 'Updateurl', '更新URL', 1, '', 0, 23, 3);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_telecomphone_detect`
--

DROP TABLE IF EXISTS `tbl_telecomphone_detect`;
CREATE TABLE IF NOT EXISTS `tbl_telecomphone_detect` (
  `telecomphonedetect_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '智能电话检测结果表ID',
  `telecomphoneinfo_id` int(11) unsigned NOT NULL COMMENT '智能电话信息表ID',
  `telecomphonedetect_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '智能电话检测类型：0为自检；1为巡检',
  `telecomphonedetect_sign` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '信号量：0表示无需立即自检；1表示需要立即自检。该字段对巡检无意义',
  `telecomphonedetect_result` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '智能电话检测结果：0为正常；1为检测中；2为未知；3为故障',
  `telecomphonedetect_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '智能电话检测时间',
  PRIMARY KEY (`telecomphonedetect_id`),
  KEY `FK_Reference_31` (`telecomphoneinfo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='智能电话检测结果表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tbl_telecomphone_detect`
--

INSERT INTO `tbl_telecomphone_detect` (`telecomphonedetect_id`, `telecomphoneinfo_id`, `telecomphonedetect_type`, `telecomphonedetect_sign`, `telecomphonedetect_result`, `telecomphonedetect_time`) VALUES
(1, 1, 0, 1, 3, 1329583881);

-- --------------------------------------------------------

--
-- 表的结构 `tbl_telecomphone_info`
--

DROP TABLE IF EXISTS `tbl_telecomphone_info`;
CREATE TABLE IF NOT EXISTS `tbl_telecomphone_info` (
  `telecomphoneinfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '智能电话信息表ID',
  `userinfo_id` int(11) unsigned NOT NULL COMMENT '用户信息表ID',
  `firecallergroup_id` int(11) unsigned NOT NULL COMMENT '群呼电话组ID',
  `firecontrollergroup_id` int(11) unsigned NOT NULL COMMENT '消防责任人组ID',
  `telecomphoneinfo_name` varchar(128) DEFAULT NULL COMMENT '智能电话名称',
  `telecomphoneinfo_number` varchar(32) NOT NULL COMMENT '智能电话号码信息',
  `telecomphoneinfo_createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '智能电话安装时间',
  `telecomphoneinfo_location` varchar(128) DEFAULT NULL COMMENT '智能电话安装位置',
  PRIMARY KEY (`telecomphoneinfo_id`),
  KEY `FK_Reference_28` (`userinfo_id`),
  KEY `FK_Reference_29` (`firecallergroup_id`),
  KEY `FK_Reference_30` (`firecontrollergroup_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='智能电话信息表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `tbl_telecomphone_info`
--

INSERT INTO `tbl_telecomphone_info` (`telecomphoneinfo_id`, `userinfo_id`, `firecallergroup_id`, `firecontrollergroup_id`, `telecomphoneinfo_name`, `telecomphoneinfo_number`, `telecomphoneinfo_createtime`, `telecomphoneinfo_location`) VALUES
(1, 2, 1, 5, '安装位置', '主叫号码', 1325599627, '安装位置11');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_user_info`
--

DROP TABLE IF EXISTS `tbl_user_info`;
CREATE TABLE IF NOT EXISTS `tbl_user_info` (
  `userinfo_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户信息表ID',
  `domaininfo_id` int(11) unsigned NOT NULL COMMENT '区域信息表ID',
  `companytype_id` int(11) unsigned NOT NULL COMMENT '企业性质表ID',
  `userrole_id` int(11) unsigned NOT NULL COMMENT '用户类型表ID',
  `userinfo_name` varchar(64) NOT NULL COMMENT '用户名',
  `userinfo_passwd` varchar(64) NOT NULL COMMENT '用户密码',
  `userinfo_email` varchar(64) NOT NULL COMMENT '用户的EMAIL信息',
  `userinfo_realname` varchar(64) NOT NULL COMMENT '用户的真实姓名',
  `userinfo_question` varchar(64) NOT NULL COMMENT '找回密码时的提问问题',
  `userinfo_answer` varchar(64) NOT NULL COMMENT '找回密码时的提问问题的答案',
  `userinfo_sex` tinyint(4) NOT NULL COMMENT '用户性别：0表示男；1表示女',
  `userinfo_telephone` varchar(32) NOT NULL COMMENT '用户的固定电话',
  `userinfo_mobile` varchar(32) NOT NULL COMMENT '用户的手机号码',
  `userinfo_fax` varchar(32) DEFAULT NULL COMMENT '用户的传真信息',
  `userinfo_weburl` varchar(64) DEFAULT NULL COMMENT '用户的WEB URL 信息',
  `userinfo_address` varchar(64) NOT NULL COMMENT '用户的地址信息',
  `userinfo_logincount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户的登录次数',
  `userinfo_createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建用户的时间',
  `userinfo_logintime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户当前的登录时间',
  `userinfo_lastlogintime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户上一次登录时间',
  `userinfo_lastloginip` varchar(16) NOT NULL COMMENT '用户上一次登录的IP地址信息',
  `userinfo_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '用户当前的状态：0表示激活；1表示未激活',
  `userinfo_companyname` varchar(64) NOT NULL COMMENT '用户的公司名称信息',
  `userinfo_idcardnumber` varchar(32) NOT NULL COMMENT '用户证件(如身份证)号码信息',
  `userinfo_rankpath` varchar(64) NOT NULL COMMENT '用户的区域路径信息',
  `userinfo_provinceid` int(11) unsigned NOT NULL COMMENT '用户所属省份ID信息',
  `userinfo_firecontrollername` varchar(64) NOT NULL COMMENT '用户所在企业消防责任人姓名',
  `userinfo_firecontrollertel` varchar(32) NOT NULL COMMENT '用户所在企业消防责任人电话号码',
  PRIMARY KEY (`userinfo_id`),
  KEY `FK_Reference_19` (`domaininfo_id`),
  KEY `FK_Reference_20` (`userrole_id`),
  KEY `FK_Reference_21` (`companytype_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户信息表' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_user_info`
--

INSERT INTO `tbl_user_info` (`userinfo_id`, `domaininfo_id`, `companytype_id`, `userrole_id`, `userinfo_name`, `userinfo_passwd`, `userinfo_email`, `userinfo_realname`, `userinfo_question`, `userinfo_answer`, `userinfo_sex`, `userinfo_telephone`, `userinfo_mobile`, `userinfo_fax`, `userinfo_weburl`, `userinfo_address`, `userinfo_logincount`, `userinfo_createtime`, `userinfo_logintime`, `userinfo_lastlogintime`, `userinfo_lastloginip`, `userinfo_status`, `userinfo_companyname`, `userinfo_idcardnumber`, `userinfo_rankpath`, `userinfo_provinceid`, `userinfo_firecontrollername`, `userinfo_firecontrollertel`) VALUES
(2, 3, 2, 3, 'test', 'a77e82e112ae8a1c543a4ad6ac16caa4084d016d', 'aa@1.cim', '真实姓名1', '问题', '答案', 1, '电话', '手机号码', '传真', NULL, '地址', 74, 1321675488, 1329585560, 1329585160, '127.0.0.1', 0, '公司名称', '证件号码', '1,2', 3, '消防责任人姓名', '消防责任人电话'),
(3, 3, 2, 2, 'xiaofang', 'a77e82e112ae8a1c543a4ad6ac16caa4084d016d', '', '1', '', '', 0, '1', '1', NULL, NULL, '', 31, 1321761707, 1329586358, 1329583934, '127.0.0.1', 0, '1', '', '1,2', 3, '', ''),
(4, 3, 2, 4, 'dx', 'a77e82e112ae8a1c543a4ad6ac16caa4084d016d', '', '32', '', '', 0, '22', '2', NULL, NULL, '2', 10, 1324218222, 1329578160, 1328547123, '127.0.0.1', 0, '2', '', '1,2', 3, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `tbl_user_role`
--

DROP TABLE IF EXISTS `tbl_user_role`;
CREATE TABLE IF NOT EXISTS `tbl_user_role` (
  `userrole_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户类型表ID',
  `userrole_name` varchar(64) NOT NULL COMMENT '用户类型名称信息',
  PRIMARY KEY (`userrole_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户类型表' AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `tbl_user_role`
--

INSERT INTO `tbl_user_role` (`userrole_id`, `userrole_name`) VALUES
(2, '消防员'),
(3, '商户'),
(4, '电信用户');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
